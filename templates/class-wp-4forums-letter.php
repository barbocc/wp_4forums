<?php

/***
 * Class Wp_4Forums_Letter
 *
 * Это класс фабрика писем
 *
 *
 */

Class Wp_4Forums_Letter {
    
    //Путь к шаблону письма
    public $template = '';
    
    //Файлы приложенные к письмо какждый файл должен быть ввиде пути к нему
    public $attachments = array();
    
    //Необходимость одбрения письма
    public $approved = false;
    
    //Какие типы пиьсма могут быть
    
    /*
     * topic
     * generated_registration
     * reply
     * discussion
     * control
     */
    public $types_of_letter = array('topic','generated_registration', 'reply', 'discussion', 'control');
    
    //Выбранный тип письма
    public $type_of_letter;
    
    //Заголовок письма
    public $headers;
    
    //Тема письма
    public $subject;
    
    //
    public $from;
    
    public $to;
    
    //Текст письма
    public $message;
    

    public function __construct(){
        
    }
    
    public function  get_template($message = null, $types_of_letter = null){
        if ( $message === null ){
            $message = $this->message;
        }
        if ( $types_of_letter === null ){
            $type_of_letter = $this->type_of_letter;
        }
        
        //Извлекаем файлы шаблоны
        $header = '';
        $content = '';
        $footer = '';
        
        $letter = $header . $content .  $footer;
        
        return $letter;
    }
    
    public function send_letter($to = null, $from = null, $subject = null, $message = null, $headers = null, $attachments = array()){
        
        if ( $to === null ){
            $to = $this->to;
        }
        
        if ( $from === null ){
            $from = $this->from;
        }
        
        if ( $subject === null ){
            $subject = $this->subject;
        }

        if ( $headers === null ){
            $headers = $this->headers;
        }
        
        if ( $message === null ){
            $message = $this->message;
        }
    
        if ( $attachments === array() ){
            $attachments = $this->attachments;
        }
        
        $message = get_template($message);
        
        return ( wp_mail($to, $subject, $message, $headers, $attachments) );
    }
    
}
