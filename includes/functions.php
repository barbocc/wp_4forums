<?php
//Приводит окончания строк к одному формату ntcn
function wif_4forum_endlines($text){
//Поиск пустых строк
    $text = str_replace(array("\r\n", "\n", "\r", PHP_EOL), PHP_EOL, $text);
    $text = str_replace(array(PHP_EOL.PHP_EOL), PHP_EOL, $text);
    return $text;
}

//Показывает настройки для админа на главной
function wif_4forum_get_redline(){
    $settings_general = get_option("wif_4forum_settings_general");
    $information = '<div style="background: #f43636;">';
    if ($settings_general['stop']) $information = $information.'<p style="color: #fff;">Приостановлена синхронизация форума</p>';
    if ($settings_general['last_one']) $information =$information. '<p style="color: #fff;">Активирована синхронизация с последним сообщением</p>';
    if ($settings_general['stop_mail']) $information =$information. '<p style="color: #fff;">Приостановлена отправка увидомлений</p>';
     
     $information .= '</div>';
     return $information;
}

//мультибайотовая заглавизация первого символа
//mb first symbol to uppercase
function mb_ucfirst($string, $encoding = 'UTF-8'){
    $strlen = mb_strlen($string, $encoding);
    $firstChar = mb_substr($string, 0, 1, $encoding);
    $then = mb_substr($string, 1, $strlen - 1, $encoding);
    return mb_strtoupper($firstChar, $encoding) . $then;
}

function wif_4forum_replace_greetings($text){
    $punctuation_marks = array('.', ',', '!','?', '...', '');
    $symbols = array("\r\n", "\n", "\r", PHP_EOL, '');
    $settings_general = get_option("wif_4forum_settings_general");
    $greetings = explode(PHP_EOL, wif_4forum_endlines($settings_general['greetings']));
    foreach ($greetings as $greeting){
        foreach ($punctuation_marks as $punctuation_mark){
            foreach ($symbols as $symbol){
                $greeting = mb_strtolower($greeting);
                $text = str_replace($greeting.' '.$punctuation_mark.' '.$symbol,'', $text);
                $text = str_replace($greeting . $punctuation_mark.' '.$symbol,'', $text);
                $text = str_replace($greeting . $punctuation_mark . $symbol,'', $text);
                
                $text = str_replace(mb_ucfirst($greeting).' '.$punctuation_mark.' '.$symbol,'', $text);
                $text = str_replace(mb_ucfirst($greeting) . $punctuation_mark.' '.$symbol,'', $text);
                $text = str_replace(mb_ucfirst($greeting) . $punctuation_mark . $symbol,'', $text);
            }
        }
    }
    return $text;
}

/**
 * Decodes 7-Bit text.
 *
 * PHP seems to think that most emails are 7BIT-encoded, therefore this
 * decoding method assumes that text passed through may actually be base64-
 * encoded, quoted-printable encoded, or just plain text. Instead of passing
 * the email directly through a particular decoding function, this method
 * runs through a bunch of common encoding schemes to try to decode everything
 * and simply end up with something *resembling* plain text.
 *
 * Results are not guaranteed, but it's pretty good at what it does.
 *
 * @param $text (string)
 *   7-Bit text to convert.
 *
 * @return (string)
 *   Decoded text.
 */
function decode7Bit($text) {
    // If there are no spaces on the first line, assume that the body is
    // actually base64-encoded, and decode it.
    $lines = explode("\r\n", $text);
    $first_line_words = explode(' ', $lines[0]);
    if ($first_line_words[0] == $lines[0]) {
        $text = base64_decode($text);
    }
    
    // Manually convert common encoded characters into their UTF-8 equivalents.
    $characters = array(
        '=20' => ' ', // space.
        '=E2=80=99' => "'", // single quote.
        '=0A' => "\r\n", // line break.
        '=A0' => ' ', // non-breaking space.
        '=C2=A0' => ' ', // non-breaking space.
        "=\r\n" => '', // joined line.
        '=E2=80=A6' => '…', // ellipsis.
        '=E2=80=A2' => '•', // bullet.
    );
    
    // Loop through the encoded characters and replace any that are found.
    foreach ($characters as $key => $value) {
        $text = str_replace($key, $value, $text);
    }
    
    return $text;
}

if(!function_exists('mb_explode')) {
    function mb_explode($delimiter, $string, $limit = -1, $encoding = 'auto') {
        if(!is_array($delimiter)) {
            $delimiter = array($delimiter);
        }
        if(strtolower($encoding) === 'auto') {
            $encoding = mb_internal_encoding();
        }
        if(is_array($string) || $string instanceof Traversable) {
            $result = array();
            foreach($string as $key => $val) {
                $result[$key] = mb_explode($delimiter, $val, $limit, $encoding);
            }
            return $result;
        }
        $result = array();
        $currentpos = 0;
        $string_length = mb_strlen($string, $encoding);
        while($limit < 0 || count($result) < $limit) {
            $minpos = $string_length;
            $delim_index = null;
            foreach($delimiter as $index => $delim) {
                if(($findpos = mb_strpos($string, $delim, $currentpos, $encoding)) !== false) {
                    if($findpos < $minpos) {
                        $minpos = $findpos;
                        $delim_index = $index;
                    }
                }
            }
            $result[] = mb_substr($string, $currentpos, $minpos - $currentpos, $encoding);
            if($delim_index === null) {
                break;
            }
            $currentpos = $minpos + mb_strlen($delimiter[$delim_index], $encoding);
        }
        return $result;
    }
}

function wif_4forum_send_mail($email, $letter, $subject){
    //Заголовки письем по стандарту
    $headers[] = 'From: obustroeno.com <no-reply@obustroeno.com>';
    $headers[] = 'Content-type: text/html';
    
    //Свёрстанная шапка письма
    $header = '<header style="height: 100px; border-bottom: solid 2px #83bc37; padding-left: 25px; padding-right: 25px;">
        <img src="http://obustroeno.com/wp-content/themes/1brus_mag/img/logo.png" alt="Логотип" style="width: 192px; margin-top: 20px;"/>
        <img src="http://obustroeno.com/wp-content/themes/1brus_mag/img/letter.png" alt="Логотип" style="margin-top: 25px; float: right;"/>
    </header>
    <div style="padding-bottom: 40px;padding-left: 25px;padding-right: 25px; font-family: \'Helvetica Neue\', Arial, Helvetica, \'Nimbus Sans L\', sans-serif;">';
    
    $settings_general = get_option( "wif_4forum_settings_general" );
    //если стоит галочка не отправлять на почту, то не отправляем
    if (!$settings_general['stop_mail']){
        return wp_mail($email, $subject, $header . $letter, $headers);
    }
}


if(!function_exists('get_the_user_ip')) {
    function get_the_user_ip()
    {
        
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        
        //for wordpress
        return apply_filters( 'wpb_get_ip', $ip );
        //for all other
        //return $ip;
    }
}

?>