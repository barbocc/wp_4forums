<?php
add_action('admin_menu', 'wif_4forum_create_menu');
add_action( 'init', 'wif_4forum_admin_init' );
//Функция инициализации и создания настроек
//add_action( 'admin_enqueue_scripts', 'wif_4forum_admin_enqueue_scripts');
/*
if (is_admin()) add_action( 'wp_enqueue_scripts', 'ajax_test_enqueue_scripts' );
function ajax_test_enqueue_scripts() {
    wp_enqueue_script( 'love', plugins_url( '/love.js', __FILE__ ), array('jquery'), '1.0', true );

    wp_localize_script( 'love', 'postlove', array(
        'ajax_url' => admin_url( 'admin-ajax.php' )
	));

}
*/

//action of enqueue scripts on wordpress console
add_action( 'admin_enqueue_scripts', 'wif_4forum_admin_enqueue_scripts');

//action of enqueue scripts on frontend
//add_action( 'wp_enqueue_scripts', 'wif_4forum_enqueue_scripts' );

//ajax action
add_action('wp_ajax_get_id_of_forum', 'wif_4forum_the_ajax_get_id_of_forum');

function wif_4forum_admin_enqueue_scripts(){
    //Встаивание скриптов на странице настроек
    wp_enqueue_script( 'wif_4forum_admin_ajax',
        WIF_4FORUM_URL . '/assets/js/wif_4forum_admin_ajax.js',
        array('jquery'), '1.0', true
    );
    
    wp_localize_script( 'wif_4forum_admin_ajax',
        'the_ajax_script',
        array('ajaxurl' => admin_url( 'admin-ajax.php' )
	));
}

function wif_4forum_the_ajax_get_id_of_forum(){
    //Do something
    if (is_int((int)$_POST['forumid_from_ajx'])) get_option_of_topic_by_forum($_POST['forumid_from_ajx']);

}

function wif_4forum_admin_init() {
    //Лог ошибок
    $errors_log = get_option("wif_4forum_errors_log");
    if (empty($errors_log)){
        $errors_log = '';
        add_option( "wif_4forum_errors_log", $errors_log, '', 'yes' );
    }

    //Создание страници с которой происходит синхронизация
    $page = get_page_by_title('wif_4forum_4sync_page');
    if (!$page){
        wp_insert_post(
            array(
            'post_content' => '[wif_4forum_4sync_page]',
            'post_title' => 'wif_4forum_4sync_page',
            'post_type'  => 'page',
            'post_name'  => 'wif_4forum_4sync_page',
            'post_status'   => 'publish'
            )
        );
    }

    //Основные настройки
    $settings_general = get_option( "wif_4forum_settings_general" );
    if( empty($settings_general) ){
        $settings_general = array(
            'email' => '',
            'pass' => '',
            'imap_path' => '{imap.beget.ru:993/imap/ssl}INBOX',
            'imap_path_to' => 'INBOX.processed',
            'stop' => NULL,
            'last_one' => NULL,
            'stop_mail' => NULL,
            'default-forum' => '',
            'min_length' => '10',
            'greetings' => ''
        );
        add_option( "wif_4forum_settings_general", $settings_general, '', 'yes' );
    }

    //Настройка обсуждений
    $settings_discus = get_option( "wif_4forum_settings_discus" );
    if( empty($settings_discus) ){
        $settings_discus = array(
            'email' => '',
            'pass' => '',
            'imap_path' => '{imap.beget.ru:993/imap/ssl}INBOX',
            //'imap_path_from' => 'INBOX.moderated',
            'imap_path_to' => 'INBOX.processed',
            'last_one' => NULL,
            'stop_mail' => NULL,
            'enable_responses' => NULL,
            'break_words' => '',
            'min_length' => '10',
            'forum' => null,
            'discus' => null
        );
        add_option( "wif_4forum_settings_discus", $settings_discus, '', 'yes' );
    }
    
    //Настройка ответов
    $settings_reply = get_option( "wif_4forum_settings_reply" );
    if( empty($settings_reply) ){
        $settings_reply = array(
            'email' => '',
            'pass' => '',
            'imap_path' => '{imap.beget.ru:993/imap/ssl}INBOX',
            'imap_path_to' => 'INBOX.processed',
            'last_one' => NULL,
            'stop_mail' => NULL,
            'enable_responses' => NULL,
            'break_words' => '',
            'fill_names_from_reply'=> NULL,
            'min_length' => '10'
        );
        add_option( "wif_4forum_settings_reply", $settings_reply, '', 'yes' );
    }

    //Настрока автораспределения форума
    $settings_def_forum = get_option("wif_4forum_settings_default_forum");
    if(empty($settings_def_forum)){
        $settings_def_forum = array(
            //'default-forum' => '',
        );
        add_option( "wif_4forum_settings_general", $settings_def_forum, '', 'yes' );
    }

    //Настройка выплат для форума
    $settings_payment = get_option("wif_4forum_settings_default_payment");
    if(empty($settings_payment)){
        $settings_payment = array(
            'short' => '200',
            'reply' => '600',
            'long' => '1000',
            'short_payment' => '10',
            'reply_payment' => '30',
            'long_payment' => '50'
        );
        add_option("wif_4forum_settings_default_payment", $settings_payment, '', 'yes');
    }
    
    //Настройка выплат для блога
    $settings_blog_payment = get_option("wif_4forum_settings_default_blog_payment");
    if(empty($settings_blog_payment)){
        $settings_blog_payment = array(
            'blog_payment' => '100',
            'image_blog_payment' => '40',
            'max_payment' => '600'
        );
        add_option("wif_4forum_settings_default_blog_payment", $settings_blog_payment, '', 'yes');
    }

    //Настройка опции для каждого эксперта
    $args = array(
        'role' => 'expert',
        'orderby' => 'user_nicename',
        'order' => 'ASC'
    );

    $experts = get_users($args);
    foreach ($experts as $expert){
        $settings_user = get_option( "wif_4forum_settings_user_". $expert->ID);
        if(empty($settings_user)){
            $settings_user = array(
                'short_payment' => $settings_payment['short_payment'],
                'reply_payment' => $settings_payment['reply_payment'],
                'long_payment' => $settings_payment['long_payment'],
            );
            add_option( "wif_4forum_settings_user_". $expert->ID, $settings_user, '', 'yes' );
        }
    }
    
    $experts = get_users($args);
    foreach ($experts as $expert){
        $settings_user = get_option( "wif_4forum_blog_user_". $expert->ID);
        if(empty($settings_user)){
            $settings_user = array(
                'blog_payment' => $settings_blog_payment['blog_payment'],
                'image_blog_payment' => $settings_blog_payment['image_blog_payment'],
                'max_payment' => $settings_blog_payment['max_payment']
            );
            add_option( "wif_4forum_blog_user_". $expert->ID, $settings_user, '', 'yes' );
        }
    }
    
}

function wif_4forum_create_menu()
{
    //create new top-level menu
    add_menu_page('WIF For Forum Settings', 'WIF For Forum Settings', 'for_forum_work', __FILE__, 'wif_4forum_settings_page', '');
}

function wif_4forum_admin_tabs( $current = 'homepage' ){

    global $current_user;

if ($current_user->roles[0]=='administrator') {
    $tabs = array(
                    'statistic' => 'Статистика',
                    'full_statistic_forum' => 'Cтатистика форума',
                    'full_statistic_blog' => 'Cтатистика блогов',
                    'general' => 'Основные настройки',
                    'reply' => 'Настройка парсинга ответов',
                    'discus' => 'Настройка обсуждений',
                    'forum' => 'Настройка автораспределения',
                    'blog_payment' => 'Оплата блогов',
                    'forum_payment' => 'Оплата форума',
                    'help' => 'Справка'
                );
    }
    else if ($current_user->roles[0]=='expert'){
        $tabs = array(
                    'statistic' => 'Статистика',
                    'full_statistic_forum' => 'Cтатистика форума',
                    'full_statistic_blog' => 'Cтатистика блогов'
                );
    }

    echo '<div id="icon-themes" class="icon32"><br></div>';
    echo '<h2 class="nav-tab-wrapper">';
    foreach( $tabs as $tab => $name ){
        $class = ( $tab == $current ) ? ' nav-tab-active' : '';
        echo "<a class='nav-tab$class' href='?page=wp_4forums%2Fincludes%2Fsettings.php&tab=$tab'>$name</a>";

    }
    echo '</h2>';
}

function wif_4forum_settings_page(){
global $current_user;
?>

<div class="wrap">
    <h2>Настройки плагина раскрутки форума</h2>
    <?php
    //wif_4forum_replace_ugly_names();
    ?>
    <?php
    if ( 'true' == esc_attr( $_GET['updated'] )){
        echo '<div class="updated" ><p>Theme Settings updated.</p></div>';
    }
    if ( isset ( $_GET['tab'] ) ) {
        wif_4forum_admin_tabs($_GET['tab']);
    }
    else {
        wif_4forum_admin_tabs('statistic');
    }
    ?>
    <div id="poststuff">
            <?php
                if ( isset ( $_GET['tab'] ) ) $tab = $_GET['tab'];
                else $tab = 'statistic';

                echo '<table class="form-table">';
                switch ($tab){
                    case 'general' :
                        if ($current_user->roles[0]=='administrator'){
                            wif_4forum_general_settings_tab();
                        }
                        break;
                    case 'reply' :
                        if ($current_user->roles[0]=='administrator'){
                            wif_4form_reply_settings_tab();
                        }
                        break;
                    case 'discus' :
                        if ($current_user->roles[0]=='administrator'){
                            wif_4form_discus_settings_tab();
                        }
                        break;
                    case 'statistic' :
                        if ($current_user->roles[0]=='administrator'){
                                wif_4forum_statistic_tab(true);
                            }
                        else if ($current_user->roles[0]=='expert'){
                                wif_4forum_statistic_tab(false);
                            }
                        break;
                    case 'full_statistic_forum' :
                        if ($current_user->roles[0]=='administrator'){
                            wif_4forum_full_statistic_tab(true);
                        }
                        else if ($current_user->roles[0]=='expert'){
                            wif_4forum_full_statistic_tab(false);
                        }
                        break;
                    case 'full_statistic_blog' :
                        if ($current_user->roles[0]=='administrator'){
                            wif_4forum_full_statistic_blog_tab(true);
                        }
                        else if ($current_user->roles[0]=='expert'){
                            wif_4forum_full_statistic_blog_tab(false);
                        }
                        break;
                     case 'blog_payment' :
                         
                         if ($current_user->roles[0]=='administrator'){
                            wif_4forum_payment_for_blog_post_settings_tab();
                        }
                        break;
                    case 'forum_payment' :
                        if ($current_user->roles[0]=='administrator'){
                            wif_4forum_payment_for_replyes_settings_tab();
                        }
                        break;
                        
                    case 'forum' :
                        if ($current_user->roles[0]=='administrator'){
                            wif_4forum_forum_settings_tab();
                        }
                        break;
                    case 'help' :
                        if ($current_user->roles[0]=='administrator'){
                            wif_4forum_help_settings_tab();
                        }
                        else if ($current_user->roles[0] == 'expert'){
                            wif_4forum_help_tab_exp();
                        }
                        break;
                }
                echo '</table>';
            ?>
    </div>
    <?php
}

function get_option_of_topic_by_forum($forum_id, $discussion_id = 0){
    
    $args = array(
        'post_type'   => 'topic',
        'post_parent' => $forum_id,
        'post_status' => 'publish',
        'orderby'     => 'date',
        'numberposts' => -1
    );
    
    $topics = get_posts($args);
    ?>
   
    <option value="0"<?php selected($discussion_id, 0); ?>>Выбрать тему</option>
    <?php
    if ($forum_id !== null || $forum_id!=='0' || $forum_id!==0){
    foreach ($topics as $topic){?>
        <option value="<?= $topic->ID ?>" <?php selected($discussion_id, $topic->ID); ?>>
        <?= $topic->post_title ?>
        </option>
    <?php }
    }?>
    
    <?php
}

function wif_4form_discus_settings_tab(){
    $settings_discus = get_option("wif_4forum_settings_discus");
    //var_dump($settings_discus);
    $errors_log = get_option("wif_4forum_errors_log");
    if ($_POST['save_discus_settings'] == '1') {
        //var_dump($_POST);
        $options = $_POST['settings_discus'];
        foreach ($settings_discus as $k=>$v)
        {
            $settings_discus[$k] = $options[$k];
        }
        update_option('wif_4forum_settings_discus', $settings_discus);
        echo 'Сохранено';
    }
    else if ($_POST['sync_discus'] == '1') {
        echo 'Синхронизация...';
        wif_4forum_synс_discus();
        echo 'Синхронизировано';
    }

    else if($_POST['save_errs'] == '1'){
        update_option('wif_4forum_errors_log', '');
    }

    ?>
    <div class="form-wrap">
    <form method="post" action="<?php admin_url(''); ?>" name="form_discus_settings" id="form_discus_settings">
        <fieldset>
            <?php
            $type = 'forum';
            $args= array(
                'post_type' => $type,
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'caller_get_posts'=> 1
            );
            $forums = get_posts($args);
            ?>
            <table>
                <tbody>
                <tr>
                    <td><p><b>Email:</b></p></td>

                    <td class="form-field form-required term-name-wrap"><label><input type="text" name="settings_discus[email]" value="<?=$settings_discus['email']?>">
                            <br><p class="description">Адрес электонной почты, с которой будет проходить синхронизация</p>
                        </label></td>
                </tr>
                <tr>
                    <td><p><b>Pass:</b></p></td>
                    <td class="form-field form-required term-name-wrap"><label><input type="text" name="settings_discus[pass]" value="<?=$settings_discus['pass']?>"><br><p class="description">Пароль электонной почты, с которой будет проходить синхронизация</p></label></td>
                </tr>
                <tr>
                    <td><p><b>Раздел извлекаемой почты</b></p></td>
                    <td class="form-field form-required term-name-wrap"><label><input type="text" name="settings_discus[imap_path]" value="<?=$settings_discus['imap_path']?>"><br><p class="description">Настройка пути к разделам почтового ящика.</p></label></td>
                </tr>
                <tr>
                    <td><p><b>Раздел места назначения:</b></p></td>
                    <td class="form-field form-required term-name-wrap"><label><input type="text" name="settings_discus[imap_path_to]" value="<?=$settings_discus['imap_path_to']?>"><br><p class="description">Место куда перемещаем письма.</p></label></td>
                </tr>
                <tr>
                    <td><p><b>Раздел форума:</b></p></td>
                    <td class="form-field form-required term-name-wrap"><label>
                        <select name="settings_discus[forum]" id="wif_4forum_select_forum4disc">
                        <option value="0" <?php selected($settings_discus['forum'], 0); ?>>Выбрать форум</option>
                         <?php foreach ($forums as $forum){?>
                            <option value="<?= $forum->ID ?>" <?php selected($settings_discus['forum'], $forum->ID); ?>>
                            <?= $forum->post_title ?>
                            </option>
                         <?php } ?>
                        </select>
                    <br><p class="description">Раздел форума для обсуждения.</p></label></td>
                </tr>
                
                <tr>
                    <td><p><b>Раздел форума:</b></p></td>
                    <td class="form-field form-required term-name-wrap"><label>
                     <select name="settings_discus[discus]" id="discus_select">
                        <?php
                        get_option_of_topic_by_forum($settings_discus['forum'], $settings_discus['discus']);?>
                    <br><p class="description">Тема для обсуждения.</p></label></td>
                    </select>
                </tr>
                
                <tr>
                    <td><p><b></b></p></td>
                    <td class="form-field form-required term-name-wrap"><label>
                            <input type="checkbox" name="settings_discus[enable_responses]" value="1" <?php checked($settings_discus['enable_responses'], true);?>>
                            <br><p class="description">Активировать синхронизацию почтового ящика ответов от пользователей</p></label></td>
                </tr>
                <tr>
                    <td><p><b></b></p></td>
                    <td class="form-field form-required term-name-wrap"><label>
                            <input type="checkbox" name="settings_discus[last_one]" value="1" <?php checked($settings_discus['last_one'], true);?>>
                            <br><p class="description">Синхронизировать последнее сообщение с почтового ящика</p></label></td>
                </tr>
                <tr>
                    <td><p><b></b></p></td>
                    <td class="form-field form-required term-name-wrap"><label>
                            <input type="checkbox" name="settings_discus[stop_mail]" value="1" <?php checked($settings_discus['stop_mail'], true);?>>
                            <br><p class="description">Прекратить отправку уведомлений экспертам на почту</p></label></td>
                </tr>

                <tr>
                    <td><p><b>Миниальный размер:</b></p></td>
                    <td class="form-field form-required term-name-wrap"><label><input type="number" min="1" step="1" name="settings_discus[min_length]" value="<?=$settings_discus['min_length']?>"><br><p class="description">Минимальный размер автоматически создаваемого топика</p></label></td>
                </tr>

                <tr>
                    <td>
                        <p><b>Стоп слова:</b></p>
                    </td>
                    <td class="form-field form-required term-name-wrap">
                        <textarea name="settings_discus[break_words]" form="form_discus_settings" rows="10" cols="100"><?=$settings_discus['break_words']?></textarea>
                        <br><p class="description">Стоп слова -- это слова после, которых извлечение текста письма прекращается. Каждое слоово словосочетание начинается со следующей строки. Весь текст до одного из этих словосчетаний будет опубликован. Если ниодного из словосочетаний не найдено, значит до строчки с датой. Если не найдено строчки с датой, тогда блока с цитированием предыдущего письма. Если ничего не найдено будет опубликован весь текст письма.</p></label>
                    </td>
                </tr>
                </tbody>

            </table>

            <p class="submit" style="clear: both;">
                <input type="submit" name="Submit"  class="button-primary" value="Сохранить" />
            </p>
            <input type="hidden" name="save_discus_settings" value="1">

    </form>
</div>
    <form method="post" action="<?php admin_url( 'themes.php?page=theme-settings' ); ?>">
        <p class="submit" style="clear: both;">
            <b><span style="color: red">ВНИМАНИЕ!</span> Для успешной синхронизации необходимо предварительно настроить автораспределение по разделам форума:</b><br><br>
            <input type="submit" name="Submit"  class="button-primary" value="Принудительная синхронизация" />
        </p>
        <input type="hidden" name="sync_discus" value="1">
    </form>

    <form method="post" action="<?php admin_url( 'themes.php?page=theme-settings' ); ?>" id="form_errors_log" >
        <p class="submit" style="clear: both;">
            <b>Лог ошибок при синхонизации:</b><br>
            <input type="hidden" name="save_errs" value="1">
            <textarea name="errors_log" form="form_errors_log" rows="20" cols="100"><?=$errors_log?></textarea>
        </p>
        <input type="submit" name="Submit"  class="button-primary" value="Очистить лог" /><br>
        </p>
    </form>
    <?php
}

//Настройка парсинга ответов
function wif_4form_reply_settings_tab(){
    $settings_reply = get_option("wif_4forum_settings_reply");
    //var_dump($settings_reply);
    $errors_log = get_option("wif_4forum_errors_log");
    if ($_POST['save_reply_settings'] == '1') {
        //var_dump($_POST);
        $options = $_POST['settings_reply'];
        foreach ($settings_reply as $k=>$v)
        {
            $settings_reply[$k] = $options[$k];
        }
        update_option('wif_4forum_settings_reply', $settings_reply);
        echo 'Сохранено';
    }
    else if ($_POST['sync_reply'] == '1') {
        echo 'Синхронизация...';
        wif_4forum_synс_replies();
        echo 'Синхронизировано';
    }

    else if($_POST['save_errs'] == '1'){
        update_option('wif_4forum_errors_log', '');
    }

    ?>
    <div class="form-wrap">
    <form method="post" action="<?php admin_url(''); ?>" name="form_reply_settings" id="form_reply_settings">
        <fieldset>
            <?php
            $type = 'forum';
            $args=array(
                'post_type' => $type,
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'caller_get_posts'=> 1
            );
            $posts = get_posts($args);
            ?>
            <table>
                <tbody>
                <tr>
                    <td><p><b>Email:</b></p></td>

                    <td class="form-field form-required term-name-wrap"><label><input type="text" name="settings_reply[email]" value="<?=$settings_reply['email']?>">
                            <br><p class="description">Адрес электонной почты, с которой будет проходить синхронизация</p>
                        </label></td>
                </tr>
                <tr>
                    <td><p><b>Pass:</b></p></td>
                    <td class="form-field form-required term-name-wrap"><label><input type="text" name="settings_reply[pass]" value="<?=$settings_reply['pass']?>"><br><p class="description">Пароль электонной почты, с которой будет проходить синхронизация</p></label></td>
                </tr>
                <tr>
                    <td><p><b>Раздел извлекаемой почты:</b></p></td>
                    <td class="form-field form-required term-name-wrap"><label><input type="text" name="settings_reply[imap_path]" value="<?=$settings_reply['imap_path']?>"><br><p class="description">Место окуда извлекаем письма.</p></label></td>
                </tr>
                <tr>
                    <td><p><b>Раздел места назначения:</b></p></td>
                    <td class="form-field form-required term-name-wrap"><label><input type="text" name="settings_reply[imap_path_to]" value="<?=$settings_reply['imap_path_to']?>"><br><p class="description">Место куда перемещаем письма.</p></label></td>
                </tr>
                <tr>
                    <td><p><b></b></p></td>
                    <td class="form-field form-required term-name-wrap"><label>
                            <input type="checkbox" name="settings_reply[enable_responses]" value="1" <?php checked($settings_reply['enable_responses'], true);?>>
                            <br><p class="description">Активировать синхронизацию почтового ящика ответов от пользователей</p></label></td>
                </tr>
                <tr>
                    <td><p><b></b></p></td>
                    <td class="form-field form-required term-name-wrap"><label>
                            <input type="checkbox" name="settings_reply[fill_names_from_reply]" value="1" <?php checked($settings_reply['fill_names_from_reply'], true);?>>
                            <br><p class="description">Автозаполнение имени и фамилии из письма пользователя, если они не были заполнены.</p></label></td>
                </tr>
                <tr>
                    <td><p><b></b></p></td>
                    <td class="form-field form-required term-name-wrap"><label>
                            <input type="checkbox" name="settings_reply[last_one]" value="1" <?php checked($settings_reply['last_one'], true);?>>
                            <br><p class="description">Синхронизировать последнее сообщение с почтового ящика</p></label></td>
                </tr>
                <tr>
                    <td><p><b></b></p></td>
                    <td class="form-field form-required term-name-wrap"><label>
                            <input type="checkbox" name="settings_reply[stop_mail]" value="1" <?php checked($settings_reply['stop_mail'], true);?>>
                            <br><p class="description">Прекратить отправку уведомлений экспертам на почту</p></label></td>
                </tr>

                <tr>
                    <td><p><b>Миниальный размер:</b></p></td>
                    <td class="form-field form-required term-name-wrap"><label><input type="number" min="1" step="1" name="settings_reply[min_length]" value="<?=$settings_reply['min_length']?>"><br><p class="description">Минимальный размер автоматически создаваемого топика</p></label></td>
                </tr>

                <tr>
                    <td>
                        <p><b>Стоп слова:</b></p>
                    </td>
                    <td class="form-field form-required term-name-wrap">
                        <textarea name="settings_reply[break_words]" form="form_reply_settings" rows="10" cols="100"><?=$settings_reply['break_words']?></textarea>
                        <br><p class="description">Стоп слова -- это слова после, которых извлечение текста письма прекращается. Каждое слоово словосочетание начинается со следующей строки. Весь текст до одного из этих словосчетаний будет опубликован. Если ниодного из словосочетаний не найдено, значит до строчки с датой. Если не найдено строчки с датой, тогда блока с цитированием предыдущего письма. Если ничего не найдено будет опубликован весь текст письма.</p></label>
                    </td>
                </tr>
                </tbody>

            </table>

            <p class="submit" style="clear: both;">
                <input type="submit" name="Submit"  class="button-primary" value="Сохранить" />
            </p>
            <input type="hidden" name="save_reply_settings" value="1">

    </form>
</div>
    <form method="post" action="<?php admin_url( 'themes.php?page=theme-settings' ); ?>">
        <p class="submit" style="clear: both;">
            <b><span style="color: red">ВНИМАНИЕ!</span> Для успешной синхронизации необходимо предварительно настроить автораспределение по разделам форума:</b><br><br>
            <input type="submit" name="Submit"  class="button-primary" value="Принудительная синхронизация" />
        </p>
        <input type="hidden" name="sync_reply" value="1">
    </form>

    <form method="post" action="<?php admin_url( 'themes.php?page=theme-settings' ); ?>" id="form_errors_log" >
        <p class="submit" style="clear: both;">
            <b>Лог ошибок при синхонизации:</b><br>
            <input type="hidden" name="save_errs" value="1">
            <textarea name="errors_log" form="form_errors_log" rows="20" cols="100"><?=$errors_log?></textarea>
        </p>
        <input type="submit" name="Submit"  class="button-primary" value="Очистить лог" /><br>
        </p>
    </form>

    <?php
}

function wif_4forum_help_settings_tab(){
    ?>
    <h1>Настройка CRON синхронизации в BEGET</h1>
    <img src="<?=WIF_4FORUM_URL?>/pic/screen1.png">
    <img src="<?=WIF_4FORUM_URL?>/pic/screen2.png">
    <img src="<?=WIF_4FORUM_URL?>/pic/screen3.png">
    <img src="<?=WIF_4FORUM_URL?>/pic/screen4.png">
    <img src="<?=WIF_4FORUM_URL?>/pic/screen5.png">
    <img src="<?=WIF_4FORUM_URL?>/pic/screen6.png">
    <img src="<?=WIF_4FORUM_URL?>/pic/screen7.png">
    <img src="<?=WIF_4FORUM_URL?>/pic/screen8.png">
    <?php
}

function wif_4forum_forum_settings_tab(){
    $last_id = get_option('wif_4forum_last_id');
    if (empty($last_id)) {
        $last_id = 0;
        add_option("wif_4forum_last_id", $last_id, '', 'yes');
    }
    if (!empty($_POST['delete_forums']) && ($_POST['delete_forums']!="")){
        $ids = explode(',',$_POST['delete_forums'],-1);

        foreach ($ids as $id){
            if ($id<$last_id){
                delete_option('wif_4forum_forum_'.$id);
            }
        }
        //var_dump($_POST['delete_forums']);
    }
    else if($_POST['save_forums']=='1'){

        for ($i = 0; $i <= $last_id; $i++){
            $_POST['wif_4forum_forum_' . $i]['keys'] = ltrim($_POST['wif_4forum_forum_' . $i]['keys']);
            $_POST['wif_4forum_forum_' . $i]['keys'] = mb_convert_case($_POST['wif_4forum_forum_' . $i]['keys'], MB_CASE_LOWER, "UTF-8");
            if (($_POST['wif_4forum_forum_' . $i]['keys']!=="") && ($_POST['wif_4forum_forum_' . $i]['forum_id']!=="")) {
                update_option('wif_4forum_forum_' . $i, $_POST['wif_4forum_forum_' . $i]);
            }
        }
        if (($_POST['wif_4forum_forum_' . $last_id]['keys']!=='')&&($_POST['wif_4forum_forum_' . $last_id]['forum_id']!=="")){
            add_option("wif_4forum_forum_" . $last_id, '', '', 'yes');
            $last_id = $last_id + 1;
            update_option('wif_4forum_last_id', $last_id);
        }
    }
    ?>

    <h1>Настройка автораспределения в разделы форума по ключевым словам</h1>
    <form method="post" action="<?php admin_url( 'themes.php?page=theme-settings' ); ?>" style="float: left;">
        <input type="hidden" id="forums_id_for_delete" name="delete_forums" value="xxx">
            <input style="background: darkred; -webkit-box-shadow: 0 1px 0 #670a00; box-shadow: 0 1px 0 #670a00;
            border-color: #670a00; text-shadow: 0 -1px 1px #670a00,1px 0 1px #670a00,0 1px 1px #670a00,-1px 0 1px #670a00;
            " type="submit" name="Submit" class="button-primary" value="Удалить" />
    </form>
    <form method="post" action="<?php admin_url( 'themes.php?page=theme-settings' ); ?>"  style="float: left;">
            <input type="submit" name="Submit"  class="button-primary" value="Сохранить" style="margin-left: 10px;"/>
        <input type="hidden" name="save_forums" value="1">
        <table  style="border-collapse: collapse; border: 1px solid #000;">
            <tbody style="background: #fff;">
            <tr>
                <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">
                    <input type="checkbox" id="delete_forum_all" name="delete_forum_all" value="true" style="margin-left: 20px;">
                </td>
                <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">
                    <b>ID Условия</b>
                </td>
                <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">
                    <b>Раздел форума</b>
                </td>
                <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">
                    <b>Ключевые слова</b>
                </td>
                <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">
                    <b>Тип условия</b>
                </td>
            </tr>
            <?php
            $last_id = get_option('wif_4forum_last_id');
            $type = 'forum';
            $args=array(
                'post_type' => $type,
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'caller_get_posts'=> 1
            );
            $posts = get_posts($args);
            for ($i = $last_id; $i>=0; $i=$i-1){
                $forum_options = get_option('wif_4forum_forum_' . $i);
                if ($forum_options || $i == $last_id) {
                    ?>
                    <tr>
                        <td style="border: 1px solid #000;"><input type="checkbox" class="delete_forum"
                            name="delete_forum[<?=$i?>]" value="<?=$i?>" style="margin-left: 30px; margin-top: 10px;"></td>
                        <td style="border: 1px solid #000; padding-left: 10px;">
                            <p style="margin-top: 8px;"><?= $i ?></p>
                        </td>
                        <td style="border: 1px solid #000;">
                            <select name = "wif_4forum_forum_<?=$i?>[forum_id]" >
                                <option value = "" <?php selected($forum_options['forum_id'], ''); ?>>Не выбрано</option>
                                <?php
                                foreach ($posts as $post) {
                                    ?>
                                    <option
                                        value="<?= $post->ID ?>" <?php selected($forum_options['forum_id'], $post->ID); ?>>
                                        <?= $post->post_title ?>
                                    </option>
                                    <?php
                                }
                            ?>
                            </select>
                        </td>

                        <td style="border: 1px solid #000;">
                            <input type="text" name="wif_4forum_forum_<?=$i?>[keys]"
                                   value="<?=$forum_options['keys']?>">
                        </td>


                        <td style="border: 1px solid #000; padding-left: 20px;">
                            <select name="wif_4forum_forum_<?=$i?>[comparekey]">
                                <option value="" <?php selected($forum_options['comparekey'], ''); ?>>ИЛИ
                                </option>
                                <option value="1" <?php selected($forum_options['comparekey'], '1'); ?>>И
                                </option>
                            </select>
                        </td>

                    </tr>
                    <?php
                }
            }
            ?>
            </tbody>
        </table>
        </form>
    <script type="text/javascript">
        jQuery(document).ready(function($){
            $('#delete_forum_all').change(function() {
                var $this = $(this);
                if ($this.is(':checked')) {
                    $( ".delete_forum" ).prop( "checked", true );
                    //$("#forums_id_for_delete").val("a/");

                } else {
                    $( ".delete_forum" ).prop( "checked", false );
                }
            });
            $('input[type=checkbox').change(function() {
                $("#forums_id_for_delete").val('');
                $('.delete_forum').each(function(i, obj) {
                    if ($(this).is(':checked')) {
                        var value = $("#forums_id_for_delete").val();
                        var value_i = $(this).val();
                        $("#forums_id_for_delete").val(value + value_i + ',');
                    }
                });
            });
            $('#to_pay').click(function(){
                $('.pay_check:checked').parent().parent().find('.for_payment_r').val('0');
            });
        });
    </script>
<?php
}

function wif_4forum_general_settings_tab(){
    $settings_general = get_option("wif_4forum_settings_general");
    //var_dump($settings_general['stop']);
    //var_dump($settings_general);
    $errors_log = get_option("wif_4forum_errors_log");
    if ($_POST['cmd'] == '1') {
    $options = $_POST['settings_general'];
    //var_dump($options);
        foreach ($settings_general as $k=>$v)
                {
                    $settings_general[$k] = $options[$k];
                }
        update_option('wif_4forum_settings_general', $settings_general);
        echo 'Сохранено';
    }
    else if ($_POST['sync'] == '1') {
        echo 'Синхронизация...';
        wif_4forum_sync();
        echo 'Синхронизировано';
    }

    else if($_POST['save_errs'] == '1'){
        update_option('wif_4forum_errors_log', '');
    }

    ?>
    <div class="form-wrap">
    <form method="post" action="<?php admin_url( 'themes.php?page=theme-settings' ); ?>" name="form_general_settings" id="form_general_settings">
        <fieldset>
    <?php
        $type = 'forum';
        $args=array(
          'post_type' => $type,
          'post_status' => 'publish',
          'posts_per_page' => -1,
          'caller_get_posts'=> 1
        );
        $posts = get_posts($args);
        ?>
        <table>
        <tbody>
            <tr>
                <td><p><b>Email:</b></p></td>

                <td class="form-field form-required term-name-wrap"><label><input type="text" name="settings_general[email]" value="<?=$settings_general['email']?>">
                        <br><p class="description">Адрес электонной почты, с которой будет проходить синхронизация</p>
                    </label></td>
            </tr>
                <tr>
                <td><p><b>Pass:</b></p></td>
                <td class="form-field form-required term-name-wrap"><label><input type="text" name="settings_general[pass]" value="<?=$settings_general['pass']?>"><br><p class="description">Пароль электонной почты, с которой будет проходить синхронизация</p></label></td>
                </tr>
            <tr>
                <td><p><b>Раздел извлекаемой почты:</b></p></td>
                <td class="form-field form-required term-name-wrap"><label><input type="text" name="settings_general[imap_path]" value="<?=$settings_general['imap_path']?>"><br><p class="description">Место окуда извлекаем письма.</p></label></td>
            </tr>
            <tr>
                <td><p><b>Раздел места назначения:</b></p></td>
                <td class="form-field form-required term-name-wrap"><label><input type="text" name="settings_general[imap_path_to]" value="<?=$settings_general['imap_path_to']?>"><br><p class="description">Место куда перемещаем письма.</p></label></td>
            </tr>
            <tr>
                <td><p><b></b></p></td>
                <td class="form-field form-required term-name-wrap"><label>
                        <input type="checkbox" name="settings_general[last_one]" value="1" <?php checked($settings_general['last_one'], true);?>>
                        <br><p class="description">Синхронизировать последнее сообщение с почтового ящика</p></label></td>
            </tr>
            <tr>
                <td><p><b></b></p></td>
                <td class="form-field form-required term-name-wrap"><label>
                        <input type="checkbox" name="settings_general[stop]" value="1" <?php checked($settings_general['stop'], true);?>>
                        <br><p class="description">Остановка синхронизации</p></label></td>
            </tr>
            <tr>
                <td><p><b></b></p></td>
                <td class="form-field form-required term-name-wrap"><label>
                        <input type="checkbox" name="settings_general[stop_mail]" value="1" <?php checked($settings_general['stop_mail'], true);?>>
                        <br><p class="description">Прекратить отправку уведомлений на почту</p></label></td>
            </tr>

            <tr>
                <td><p><b>Миниальный размер:</b></p></td>
                <td class="form-field form-required term-name-wrap"><label><input type="number" min="1" step="1" name="settings_general[min_length]" value="<?=$settings_general['min_length']?>"><br><p class="description">Минимальный размер автоматически создаваемого топика</p></label></td>
            </tr>

            <tr>
                <td>
                    <p><b>Раздел:</b></p>
                </td>
                <td class="form-field form-required term-name-wrap">
                    <select name="settings_general[default-forum]">
                    <option value="" <?php selected( $settings_general['default-forum'], '' ); ?>>Не выбрано</option>
                    <?php
                    foreach ($posts as $post){
                    ?>
                        <option value="<?=$post->ID?>" <?php selected( $settings_general['default-forum'], $post->ID ); ?>><?=$post->post_title?></option>
                    <?php
                    }
                    ?>
                    </select>
                    <p class="description">Раздел форума по умолчанию, в котором создаются темы при синхронизации, более гибкая настройка в разделе "Настройка автораспределения"</p>
                </td>
            </tr>
            
                <tr>
                    <td>
                        <p><b>Слова приветствия:</b></p>
                    </td>
                    <td class="form-field form-required term-name-wrap">
                        <textarea name="settings_general[greetings]" form="form_general_settings" rows="10" cols="100"><?=$settings_general['greetings']?></textarea>
                        <br><p class="description">Слова приветствия -- это слова после, которых начинается извлечение текста письма. Каждое слоово словосочетание начинается со следующей строки. Весь текст после одного из этих словосчетаний и знаков препинания будет опубликован.
                    </td>
                </tr>
            
        </tbody>

        </table>

                    <p class="submit" style="clear: both;">
                        <input type="submit" name="Submit"  class="button-primary" value="Сохранить" />
                    </p>
                     <input type="hidden" name="cmd" value="1">

    </form>
    </div>
        <form method="post" action="<?php admin_url( 'themes.php?page=theme-settings' ); ?>">
            <p class="submit" style="clear: both;">
                <b><span style="color: red">ВНИМАНИЕ!</span> Для успешной синхронизации необходимо предварительно настроить автораспределение по разделам форума:</b><br><br>
                <input type="submit" name="Submit"  class="button-primary" value="Принудительная синхронизация" />
            </p>
            <input type="hidden" name="sync" value="1">
        </form>

    <form method="post" action="<?php admin_url( 'themes.php?page=theme-settings' ); ?>" id="form_errors_log" >
        <p class="submit" style="clear: both;">
            <b>Лог ошибок при синхонизации:</b><br>
        <input type="hidden" name="save_errs" value="1">
        <textarea name="errors_log" form="form_errors_log" rows="20" cols="100"><?=$errors_log?></textarea>
        </p>
        <input type="submit" name="Submit"  class="button-primary" value="Очистить лог" /><br>
        </p>
    </form>

    <?php
}


function wif_4forum_full_statistic_tab($admin = true){
    $itogo['money'] = 0;
    $itogo['symbols'] = 0;
    $selected_year = date('Y');
    $expert_id = '';
    if ($admin == false) {
        $expert_id = get_current_user_id();
    }
    $selected_month = (int)date('m');
    $months = array('Все месяцы', 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');
    if ($_POST['get_full_expert_adm_stats']==1) {
        if ($admin == true) {
            $expert_id = $_POST['expert_id'];
        }
    }
    //var_dump($_POST);
    elseif($_POST['get_full_expert_exp_stats']==1) {
            //var_dump($_POST);
            $expert_id = get_current_user_id();
        }

    if ($_POST['year']) $selected_year = $_POST['year'];
    if ($_POST['month']) $selected_month = $_POST['month'];

    $usr_args = array(
        'role' => 'expert',
        'orderby' => 'user_nicename',
        'order' => 'ASC'
    );

    $experts = get_users($usr_args);

if ($admin==true){
    ?>
<form method="post" action="<?php admin_url('themes.php?page=theme-settings'); ?>" style="float: left;">
    <input type="hidden" name="get_full_expert_adm_stats" value="1">
    <select name="expert_id">
        <option value="" <?php selected($expert_id, ''); ?>>Все эксперты</option>
        <?php
        foreach ($experts as $expert) {
            ?>
            <option
                value="<?= $expert->ID ?>" <?php selected($expert_id, $expert->ID); ?>>
                <?= $expert->display_name ?>
                (<?= $expert->user_login ?>)
            </option>
            <?php
        }
        ?>
    </select>
    <?php
    }
    else{
        ?>
        <form method="post" action="<?php admin_url('themes.php?page=theme-settings'); ?>" style="float: left;">
            <input type="hidden" name="get_full_expert_exp_stats" value="1">
        <?php
    }
    ?>
    <select name = "year" >
        <option value = "" <?php selected($selected_year, ''); ?>>Все года</option>
        <?php
        for ($year=2016; $year<=date("Y");$year++) {
            ?>
            <option
                value="<?= $year ?>" <?php selected($selected_year, $year); ?>>
                <?= $year ?>
            </option>
            <?php
        }
        ?>
    </select>

    <select name = "month" >
        <?php
        for ($month=0; $month<=12;$month++) {
            ?>
            <option
                value="<?= $month ?>" <?php selected($selected_month, $month); ?>>
                <?= $months[$month] ?>
            </option>
            <?php
        }
        ?>
    </select>
    <input type="submit" name="Submit"  class="button-primary" value="Показать" style="margin-left: 10px;"/>
</form>
    <table style="border-collapse: collapse; border: 1px solid #000; margin-top: 20px;">
        <tbody style="background: #fff;">
        <tr>
            <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">
                <p>Дата</p>
            </td>
            <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;">
                <p>Тип</p>
            </td>
            <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;">
                <p>Количество символов</p>
            </td>
            <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;">
                <p>Автор</p>
            </td>
            <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;">
                <p>Содержимое</p>
            </td>
            <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;">
                <p>Оплата</p>
            </td>
        </tr>
<?php
    $args = array(
        'post_status' => 'publish',
        'post_type' => 'reply',
        //'author' => $expert_id,
        'numberposts' => -1,
        'orderby' => 'post_date',
        'order' => 'ASC',
    );
    if ($expert_id!==''){
        $args['author'] = $expert_id;
    }
    $posts = get_posts($args);
    foreach ($posts as $post){
        $data_time = explode (' ',$post->post_date);
        $data = explode('-',$data_time[0]);

        if (($selected_year!==''&&$data[0]!==$selected_year)||((int)$selected_month>0&&((int)$data[1]!==(int)$selected_month))){
            continue;
        }
        $post->lenght = get_post_meta($post->ID,'reply_length',true);
        $post_cat = get_post_meta($post->ID, 'reply_cat', true);
        $post_money = get_post_meta($post->ID, 'reply_payment', true);
        $post->payment =  $post_money;
        if ($post_cat == 'reply'){
            $post->cat = 'Средний';
        }
        elseif ($post_cat == 'short'){
            $post->cat = 'Короткий';
        }
        elseif ($post_cat == 'long'){
            $post->cat = 'Длинный';
        }
        elseif ($post_cat == 'repeat'){
            $post->cat = 'Объединён с предыдущим';
        }

        $author = get_user_by ('ID', $post->post_author);
        $post->author = $author;
        ?>
        <tr>
            <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">
                <p><?=$post->post_date?></p>
            </td>
            <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">
                <p><?=$post->cat?></p>
            </td>
            <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">
                <p><?=$post->lenght?></p>
            </td>
            <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">
                    <p><?=$post->author->display_name?> (<?= $post->author->user_login ?>)</p>
            </td>
            <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">
                <p><a href="<?=$post->guid?>"><?=mb_substr( esc_html($post->post_content) ,0, 101)?></a></p>
            </td>
            <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">
                <p><?=$post->payment?></p>
            </td>
        </tr>
        <?php
        $itogo['money'] += $post->payment;
        $itogo['symbols'] += $post->lenght;
    }
?>
        <tr>
            <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">
            <p>ИТОГО</p>
            </td>
            <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">

            </td>
            <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">
                <p><?=$itogo['symbols']?></p>
            </td>
            <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">

            </td>
            <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">

            </td>
            <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">
                <p><?=$itogo['money'] += $post->payment?></p>
            </td>
        </tr>
        </tbody>
     </table>
        <?php
}


function wif_4forum_full_statistic_blog_tab($admin = true){
    
    $itogo['money'] = 0;
    $itogo['posts'] = 0;
    $itogo['pictures'] = 0;
    
    $selected_year = date('Y');
    $expert_id = '';
    if ($admin == false) {
        $expert_id = get_current_user_id();
    }
    $selected_month = (int)date('m');
    $months = array('Все месяцы', 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');
    if ($_POST['get_full_expert_adm_stats']==1) {
        if ($admin == true) {
            $expert_id = $_POST['expert_id'];
        }
    }
    //var_dump($_POST);
    elseif($_POST['get_full_expert_exp_stats']==1) {
            //var_dump($_POST);
            $expert_id = get_current_user_id();
        }

    if ($_POST['year']) $selected_year = $_POST['year'];
    if ($_POST['month']) $selected_month = $_POST['month'];

    $usr_args = array(
        'role' => 'expert',
        'orderby' => 'user_nicename',
        'order' => 'ASC'
    );

    $experts = get_users($usr_args);

if ($admin==true){
    ?>
<form method="post" action="<?php admin_url('themes.php?page=theme-settings'); ?>" style="float: left;">
    <input type="hidden" name="get_full_expert_adm_stats" value="1">
    <select name="expert_id">
        <option value="" <?php selected($expert_id, ''); ?>>Все эксперты</option>
        <?php
        foreach ($experts as $expert) {
            ?>
            <option
                value="<?= $expert->ID ?>" <?php selected($expert_id, $expert->ID); ?>>
                <?= $expert->display_name ?>
                (<?= $expert->user_login ?>)
            </option>
            <?php
        }
        ?>
    </select>
    <?php
    }
    else{
        ?>
        <form method="post" action="<?php admin_url('themes.php?page=theme-settings'); ?>" style="float: left;">
            <input type="hidden" name="get_full_expert_exp_stats" value="1">
        <?php
    }
    ?>
    <select name = "year" >
        <option value = "" <?php selected($selected_year, ''); ?>>Все года</option>
        <?php
        for ($year=2016; $year<=date("Y");$year++) {
            ?>
            <option
                value="<?= $year ?>" <?php selected($selected_year, $year); ?>>
                <?= $year ?>
            </option>
            <?php
        }
        ?>
    </select>

    <select name = "month" >
        <?php
        for ($month=0; $month<=12;$month++) {
            ?>
            <option
                value="<?= $month ?>" <?php selected($selected_month, $month); ?>>
                <?= $months[$month] ?>
            </option>
            <?php
        }
        ?>
    </select>
    <input type="submit" name="Submit"  class="button-primary" value="Показать" style="margin-left: 10px;"/>
</form>
    <table style="border-collapse: collapse; border: 1px solid #000; margin-top: 20px;">
        <tbody style="background: #fff;">
        <tr>
            <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">
                <p>Дата</p>
            </td>
            <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;">
                <p>Количество картинок</p>
            </td>
            <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;">
                <p>Автор</p>
            </td>
            <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;">
                <p>Содержимое</p>
            </td>
            <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;">
                <p>Оплата</p>
            </td>
        </tr>
         
<?php

    $args = array(
        'post_type'   => 'post',
        //'author' => $expert_id,
        'numberposts' => -1,
        'post_status' => 'publish',
        'orderby' => 'date',
        'order' => 'ASC',
        'date_query' => array(
            array(
                'after'     => "$selected_year-$selected_month-01 - 1 day",
                'before'    => "$selected_year-$selected_month-01 + 1 month",
            ),
    ),
    );
    if ($expert_id!==''){
        $args['author'] = $expert_id;
    }
    
    $posts = get_posts($args);

    //var_dump($posts);
    
    foreach ($posts as $post){
        $data_time = explode (' ',$post->post_date);
        $data = explode('-',$data_time[0]);

        if (($selected_year!==''&&$data[0]!==$selected_year)||((int)$selected_month>0&&((int)$data[1]!==(int)$selected_month))){
            continue;
        }

        $post_pictures = get_post_meta($post->ID, 'attached_pictures_count', true);
        $post_money = get_post_meta($post->ID, 'payment', true);
        $post->payment =  $post_money;
        $post->pictures = $post_pictures;

        $author = get_user_by ('ID', $post->post_author);
        $post->author = $author;
        ?>
        <tr>
            <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">
                <p><?=$post->post_date?></p>
            </td>

            <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">
                <p><?=$post->pictures?></p>
            </td>
            <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">
                    <p><?=$post->author->display_name?> (<?= $post->author->user_login ?>)</p>
            </td>
            <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">
                <p><a href="<?=$post->guid?>"><?=$post->post_title?></a></p>
            </td>
            <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">
                <p><?=$post->payment?></p>
            </td>
        </tr>
        <?php
        $itogo['money'] += $post->payment;
        $itogo['pictures'] += $post->pictures;
        $itogo['posts'] += 1;
    }
?>
        <tr>
            <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">
            <p>ИТОГО</p>
            </td>

            <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">
               <p><?=$itogo['pictures']?></p>
            </td>
            <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">

            </td>
            <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">
                <p><?=$itogo['posts']?></p>
            </td>
            <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">
                <p><?=$itogo['money']?></p>
            </td>
        </tr>
        </tbody>
     </table>
        <?php
}

function wif_4forum_statistic_tab($admin=true){
    $selected_year = date("Y");
    $selected_month = (int)date('m');
    $months = array('Все месяцы', 'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');
    
    $expert_id = '';
    if ($admin == false){
        $expert_id = get_current_user_id();
    }
    if ($_POST['get_expert_adm_stats']){
        if ($admin==true) {
            $expert_id = $_POST['expert_id'];
        }
        else {
            $expert_id = get_current_user_id();
        }
        $selected_year = $_POST['year'];
        $selected_month = $_POST['month'];
    }

    $usr_args = array(
        'role' => 'expert',
        'orderby' => 'user_nicename',
        'order' => 'ASC'
    );

    $experts = get_users($usr_args);
    if ($admin==true) {
        ?>

        <form method="post" action="<?php admin_url('themes.php?page=theme-settings'); ?>"  style="float: left;">
        <input type="hidden" name="get_expert_adm_stats" value="1">
        <select name="expert_id">
            <option value="" <?php selected($expert_id, ''); ?>>Все эксперты</option>
            <?php
            foreach ($experts as $expert) {
                ?>
                <option
                    value="<?= $expert->ID ?>" <?php selected($expert_id, $expert->ID); ?>>
                    <?= $expert->display_name ?>
                </option>
                <?php
            }
            ?>
        </select>
        <?php
    }
        ?>
    <select name = "year" >
        <?php
        for ($year=2016; $year<=date("Y");$year++) {
            ?>
            <option
                value="<?= $year ?>" <?php selected($selected_year, $year); ?>>
                <?= $year ?>
            </option>
            <?php
        }
        ?>
    </select>
    <select name = "month" >
        <?php
        for ($month=0; $month<=12;$month++) {
            ?>
            <option
                value="<?= $month ?>" <?php selected($selected_month, $month); ?>>
                <?= $months[$month] ?>
            </option>
            <?php
        }
        ?>
    </select>
        <input type="submit" name="Submit"  class="button-primary" value="Показать" style="margin-left: 10px;"/>
</form>

        <table style="border-collapse: collapse; border: 1px solid #000;">
            <tbody style="background: #fff;">
            <tr>
               <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">
                    <p>Эксперт</p>
                </td>
                <td style="border: 1px solid #000; padding-left: 10px; padding-right: 10px;">
                    <p>Месяц</p>
                </td>
                <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;">
                    <p>Короткие</p>
                </td>
                <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;">
                    <p>Обычные</p>
                </td>
                <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;">
                    <p>Длинные</p>
                </td>
                <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;">
                    <p>Всего коментариев</p>
                </td>
                <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;">
                    <p>Сумма</p>
                </td>
            </tr>
    <?php
    
    if ($admin == true){
        $expert_id = $_POST['expert_id'];
         $usr_args = array(
            'role' => 'expert',
            'orderby' => 'user_nicename',
            'order' => 'ASC'
        );
    }
    else{
         $usr_args = array(
             'ID' => get_current_user_id(),
            'role' => 'expert',
            'orderby' => 'user_nicename',
            'order' => 'ASC'
        );
    }
    
    $experts = get_users($usr_args);
    $sum = 0;
    $sum_all_short = 0;
    $sum_all_simple = 0;
    $sum_all_long = 0;
    $sum_all_coments = 0;

    foreach ($experts as $expert){
    //if ($_POST['get_expert_stats']) {
        $args = array(
            'post_status' => 'publish',
            'post_type' => 'reply',
            'author' => $expert->ID,
            'numberposts' => -1,
            'orderby' => 'post_date',
            'order' => 'ASC',
        );
        $posts = get_posts($args);
        $year_posts = array();
        $months_stats =  array(
            'jan' => array(
                'name' => 'январь',
                'count' => 0,
                'long' => 0,
                'reply' => 0,
                'small' => 0,
                'money' => 0,
            ),
            'feb' => array(
                'name' => 'февраль',
                'count' => 0,
                'long' => 0,
                'reply' => 0,
                'small' => 0,
                'money' => 0,
            ),
            'mar' => array(
                'name' => 'март',
                'count' => 0,
                'long' => 0,
                'reply' => 0,
                'small' => 0,
                'money' => 0,
            ),
            'apr' => array(
                'name' => 'апрель',
                'count' => 0,
                'long' => 0,
                'reply' => 0,
                'small' => 0,
                'money' => 0,
            ),
            'may' => array(
                'name' => 'май',
                'count' => 0,
                'long' => 0,
                'reply' => 0,
                'small' => 0,
                'money' => 0,
            ),
            'jun' => array(
                'name' => 'июнь',
                'count' => 0,
                'long' => 0,
                'reply' => 0,
                'small' => 0,
                'money' => 0,
            ),
            'jul' => array(
                'name' => 'июль',
                'count' => 0,
                'long' => 0,
                'reply' => 0,
                'small' => 0,
                'money' => 0,
            ),
            'aug' => array(
                'name' => 'август',
                'count' => 0,
                'long' => 0,
                'reply' => 0,
                'small' => 0,
                'money' => 0,
            ),
            'sep' => array(
                'name' => 'сентябрь',
                'count' => 0,
                'long' => 0,
                'reply' => 0,
                'small' => 0,
                'money' => 0,
            ),
            'oct' => array(
                'name' => 'октябрь',
                'count' => 0,
                'long' => 0,
                'reply' => 0,
                'small' => 0,
                'money' => 0,
            ),
            'nov' => array(
                'name' => 'ноябрь',
                'count' => 0,
                'long' => 0,
                'reply' => 0,
                'small' => 0,
                'money' => 0,
            ),
            'dec' => array(
                'name' => 'декабрь',
                'count' => 0,
                'long' => 0,
                'reply' => 0,
                'small' => 0,
                'money' => 0,
            ),
        );
        
        foreach ($posts as $post) {
            $data_time = explode (' ',$post->post_date);
             //echo ($selected_month);
            $data = explode('-',$data_time[0]);
            if ($data[0]==$selected_year) {
                if ($selected_month!=='0' && (int)$data[1]!==(int)$selected_month) {
                    continue;
                };
                
                switch($data[1]){
                    case 1:
                        $post->month = 'январь';
                        $months_stats['jan']['count']++;
                        $post_cat = get_post_meta($post->ID, 'reply_cat', true);
                        $post_money = get_post_meta($post->ID, 'reply_payment', true);
                        $post->lenght = get_post_meta($post->ID,'reply_length',true);
                        $post->payment =  $post_money;
                        if ($post_cat == 'reply'){
                            $months_stats['jan']['reply']++;
                            $post->cat = 'Средний';
                        }
                        elseif ($post_cat == 'short'){
                            $months_stats['jan']['small']++;
                            $post->cat = 'Короткий';
                        }
                        elseif ($post_cat == 'long'){
                            $months_stats['jan']['long']++;
                            $post->cat = 'Длинный';
                        }
                        $months_stats['jan']['money']+=$post_money;
                        break;
                    case 2:
                        $post->month ='февраль';
                        $months_stats['feb']['count']++;
                        $post->lenght = get_post_meta($post->ID,'reply_length',true);
                        $post_cat = get_post_meta($post->ID, 'reply_cat', true);
                        $post_money = get_post_meta($post->ID, 'reply_payment', true);
                        $post->payment =  $post_money;
                        if ($post_cat == 'reply'){
                            $months_stats['feb']['reply']++;
                            $post->cat = 'Средний';
                        }
                        elseif ($post_cat == 'short'){
                            $months_stats['feb']['small']++;
                            $post->cat = 'Короткий';
                        }
                        elseif ($post_cat == 'long'){
                            $months_stats['feb']['long']++;
                            $post->cat = 'Длинный';
                        }
                        $months_stats['feb']['money']+=$post_money;
                        break;
                    case 3:
                        $post->month ='март';
                        $months_stats['mar']['count']++;
                        $post->lenght = get_post_meta($post->ID,'reply_length',true);
                        $post_cat = get_post_meta($post->ID, 'reply_cat', true);
                        $post_money = get_post_meta($post->ID, 'reply_payment', true);
                        $post->payment =  $post_money;
                        if ($post_cat == 'reply'){
                            $months_stats['mar']['reply']++;
                            $post->cat = 'Средний';
                        }
                        elseif ($post_cat == 'short'){
                            $months_stats['mar']['small']++;
                            $post->cat = 'Короткий';
                        }
                        elseif ($post_cat == 'long'){
                            $months_stats['mar']['long']++;
                            $post->cat = 'Длинный';
                        }
                        $months_stats['mar']['money']+=$post_money;
                        break;
                    case 4:
                        $post->month ='апрель';
                        $months_stats['apr']['count']++;
                        $post->lenght = get_post_meta($post->ID,'reply_length',true);
                        $post_cat = get_post_meta($post->ID, 'reply_cat', true);
                        $post_money = get_post_meta($post->ID, 'reply_payment', true);
                        $post->payment =  $post_money;
                        if ($post_cat == 'reply'){
                            $months_stats['apr']['reply']++;
                            $post->cat = 'Средний';
                        }
                        elseif ($post_cat == 'short'){
                            $months_stats['apr']['small']++;
                            $post->cat = 'Короткий';
                        }
                        elseif ($post_cat == 'long'){
                            $months_stats['apr']['long']++;
                            $post->cat = 'Длинный';
                        }
                        $months_stats['apr']['money']+=$post_money;
                        break;
                    case 5:
                        $post->month ='май';
                        $months_stats['may']['count']++;
                        $post->lenght = get_post_meta($post->ID,'reply_length',true);
                        $post_cat = get_post_meta($post->ID, 'reply_cat', true);
                        $post_money = get_post_meta($post->ID, 'reply_payment', true);
                        $post->payment =  $post_money;
                        if ($post_cat == 'reply'){
                            $months_stats['may']['reply']++;
                            $post->cat = 'Средний';
                        }
                        elseif ($post_cat == 'short'){
                            $months_stats['may']['small']++;
                            $post->cat = 'Короткий';
                        }
                        elseif ($post_cat == 'long'){
                            $months_stats['may']['long']++;
                            $post->cat = 'Длинный';
                        }
                        $months_stats['may']['money']+=$post_money;
                        break;
                    case 6:
                        $post->month ='июнь';
                        $months_stats['jun']['count']++;
                        $post->lenght = get_post_meta($post->ID,'reply_length',true);
                        $post_cat = get_post_meta($post->ID, 'reply_cat', true);
                        $post_money = get_post_meta($post->ID, 'reply_payment', true);
                        $post->payment =  $post_money;
                        if ($post_cat == 'reply'){
                            $months_stats['jun']['reply']++;
                            $post->cat = 'Средний';
                        }
                        elseif ($post_cat == 'short'){
                            $months_stats['jun']['small']++;
                            $post->cat = 'Короткий';
                        }
                        elseif ($post_cat == 'long'){
                            $months_stats['jun']['long']++;
                            $post->cat = 'Длинный';
                        }
                        $months_stats['jun']['money']+=$post_money;
                        break;
                    case 7:
                        $post->month ='июль';
                        $months_stats['jul']['count']++;
                        $post->lenght = get_post_meta($post->ID,'reply_length',true);
                        $post_cat = get_post_meta($post->ID, 'reply_cat', true);
                        $post_money = get_post_meta($post->ID, 'reply_payment', true);
                        $post->payment =  $post_money;
                        if ($post_cat == 'reply'){
                            $months_stats['jul']['reply']++;
                            $post->cat = 'Средний';
                        }
                        elseif ($post_cat == 'short'){
                            $months_stats['jul']['small']++;
                            $post->cat = 'Короткий';
                        }
                        elseif ($post_cat == 'long'){
                            $months_stats['jul']['long']++;
                            $post->cat = 'Длинный';
                        }
                        $months_stats['jul']['money']+=$post_money;
                        break;
                    case 8:
                        $post->month ='август';
                        $months_stats['aug']['count']++;
                        $post->lenght = get_post_meta($post->ID,'reply_length',true);
                        $post_cat = get_post_meta($post->ID, 'reply_cat', true);
                        $post_money = get_post_meta($post->ID, 'reply_payment', true);
                        $post->payment =  $post_money;
                        if ($post_cat == 'reply'){
                            $months_stats['aug']['reply']++;
                            $post->cat = 'Средний';
                        }
                        elseif ($post_cat == 'short'){
                            $months_stats['aug']['small']++;
                            $post->cat = 'Короткий';
                        }
                        elseif ($post_cat == 'long'){
                            $months_stats['aug']['long']++;
                            $post->cat = 'Длинный';
                        }
                        $months_stats['aug']['money']+=$post_money;
                        break;
                    case 9:
                        $post->month ='сентябрь';
                        $months_stats['sep']['count']++;
                        $post->lenght = get_post_meta($post->ID,'reply_length',true);
                        $post_cat = get_post_meta($post->ID, 'reply_cat', true);
                        $post_money = get_post_meta($post->ID, 'reply_payment', true);
                        $post->payment =  $post_money;
                        if ($post_cat == 'reply'){
                            $months_stats['sep']['reply']++;
                            $post->cat = 'Средний';
                        }
                        elseif ($post_cat == 'short'){
                            $months_stats['sep']['small']++;
                            $post->cat = 'Короткий';
                        }
                        elseif ($post_cat == 'long'){
                            $months_stats['sep']['long']++;
                            $post->cat = 'Длинный';
                        }
                        $months_stats['sep']['money']+=$post_money;
                        break;
                    case 10:
                        $post->month ='октябрь';
                        $months_stats['oct']['count']++;
                        $post->lenght = get_post_meta($post->ID,'reply_length',true);
                        $post_cat = get_post_meta($post->ID, 'reply_cat', true);
                        $post_money = get_post_meta($post->ID, 'reply_payment', true);
                        $post->payment =  $post_money;
                        if ($post_cat == 'reply'){
                            $months_stats['oct']['reply']++;
                            $post->cat = 'Средний';
                        }
                        elseif ($post_cat == 'short'){
                            $months_stats['oct']['small']++;
                            $post->cat = 'Короткий';
                        }
                        elseif ($post_cat == 'long'){
                            $months_stats['oct']['long']++;
                            $post->cat = 'Длинный';
                        }
                        $months_stats['oct']['money']+=$post_money;
                        break;
                    case 11:
                        $post->month ='ноябрь';
                        $months_stats['nov']['count']++;
                        $post->lenght = get_post_meta($post->ID,'reply_length',true);
                        $post_cat = get_post_meta($post->ID, 'reply_cat', true);
                        $post_money = get_post_meta($post->ID, 'reply_payment', true);
                        $post->payment =  $post_money;
                        if ($post_cat == 'reply'){
                            $months_stats['nov']['reply']++;
                            $post->cat = 'Средний';
                        }
                        elseif ($post_cat == 'short'){
                            $months_stats['nov']['small']++;
                            $post->cat = 'Короткий';
                        }
                        elseif ($post_cat == 'long'){
                            $months_stats['nov']['long']++;
                            $post->cat = 'Длинный';
                        }
                        $months_stats['nov']['money']+=$post_money;
                        break;

                    case 12:
                        $post->month ='декабрь';
                        $months_stats['dec']['count']++;
                        $post->lenght = get_post_meta($post->ID,'reply_length',true);
                        $post_cat = get_post_meta($post->ID, 'reply_cat', true);
                        $post_money = get_post_meta($post->ID, 'reply_payment', true);
                        $post->payment =  $post_money;
                        if ($post_cat == 'reply'){
                            $months_stats['']['reply']++;
                            $post->cat = 'Средний';
                        }
                        elseif ($post_cat == 'short'){
                            $months_stats['dec']['small']++;
                            $post->cat = 'Короткий';
                        }
                        elseif ($post_cat == 'long'){
                            $months_stats['dec']['long']++;
                            $post->cat = 'Длинный';
                        }
                        $months_stats['dec']['money']+=$post_money;
                        break;
                }
                array_push($year_posts, $post);
            }
        }
        ?>
        <?php
        foreach ($months_stats as $month) {
            if ($month['count']>0){
            ?>
                <tr>
                   <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;"><?=$expert->user_login?></td>
                    <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;">
                        <p><?=$month['name']?></p>
                    </td>
                    <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;">
                        <p><?=$month['small']?></p>
                    </td>
                    <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;">
                        <p><?=$month['reply']?></p>
                    </td>
                    <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;">
                        <p><?=$month['long']?></p>
                    </td>
                    <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;">
                        <p><?=$month['count']?></p>
                    </td>
                    <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;">
                        <p><?=$month['money']?></p>
                    </td>
                </tr>
                
            <?php
                $sum_all_short += $month['small'];
                $sum_all_simple += $month['reply'];
                $sum_all_long += $month['long'];
                $sum_all_coments += $month['count'];
                $sum += $month['money'];
            }
        }
    }
            ?>
                <tr>
                   
                    <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;">
                        <p>ИТОГО</p>
                    </td>
                   <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;">
                        <p></p>
                    </td>
                    <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;">
                        <p><?=$sum_all_short?></p>
                    </td>
                    <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;">
                        <p><?=$sum_all_simple?></p>
                    </td>
                   <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;">
                        <p><?=$sum_all_long?></p>
                    </td>
                   <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;">
                        <p><?=$sum_all_coments?></p>
                    </td>
                    <td style="border: 1px solid #000;padding-left: 10px; padding-right: 10px;">
                        <p><?=$sum?></p>
                    </td>
                </tr>
            </tbody>
        </table>
    <?php
    
}



function wif_4forum_payment_for_blog_post_settings_tab(){
     $settings_default = get_option("wif_4forum_settings_default_blog_payment");
    $args = array(
        'role' => 'expert',
        'orderby' => 'user_nicename',
        'order' => 'ASC'
    );
    $experts = get_users($args);
     //если пост команд
    if ($_POST['save_payment_blog']){
        $options = $_POST['settings_default'];
        foreach ($options as $k=>$v){
            $settings_default[$k] = $options[$k];
        }
        update_option('wif_4forum_settings_default_blog_payment', $settings_default);
        foreach ($experts as $expert){
            $settings_user = get_option( "wif_4forum_blog_user_". $expert->ID);
            
            $options = $_POST['wif_4forum_blog_user_'.$expert->ID];
            $settings_user['payed_r'] = $settings_user['payed_r'] + $settings_user['for_payment_r'];
            foreach ($options as $k=>$v){
                if ($k!='payed_r') {
                    $settings_user[$k] = $options[$k];
                }
            }
            update_option("wif_4forum_blog_user_". $expert->ID, $settings_user);
        }
    }

?>
    <h1>Настройки настройка оплаты</h1>
    <form method="post" action="<?php admin_url( 'themes.php?page=theme-settings' ); ?>">
    <table>
        <tbody>
            <tr>
                <td><h3>Настройка оплаты экспертов по умолчани</h3></td>
            </tr>
                <tr>
                    <td> Цена публикации в блоге (руб.):</td>
                    <td><input type="text" name="settings_default[blog_payment]" value="<?=$settings_default['blog_payment']?>"></td>
                </tr>
                <tr>
                    <td>Доплата за картинку (руб.):</td>
                    <td><input type="text" name="settings_default[image_blog_payment]" value="<?=$settings_default['image_blog_payment']?>"></td>
                </tr>
                <tr>
                    <td>Максимум за публикацию (руб.):</td>
                    <td><input type="text" name="settings_default[max_payment]" value="<?=$settings_default['max_payment']?>"></td>
                </tr>
        </tbody>
    <table>
        <tbody>
            <tr>
                <td>
                    <input type="submit" name="Submit"  class="button-primary" value="Сохранить" />
                    <input type="hidden" name="save_payment_blog" value="1"/>
                </td>
             </tr>
        </tbody>
    </table>

    </table>
        <br>
        <br>
        <h3>Настройка оплаты экспертов</h3>
    <table style="border-collapse: collapse; border: 1px solid #000; background: #ffffff;">
    <tbody>
    <tr>
        <td><input type="checkbox" id="pay_all" name="pay_all" value="true"></td>
        <td>
            <b>Логин</b>
        </td>
        <td>
             <b>Цена публикацию</b>
        </td>
        <td>
            <b>Цена за картинку</b>
        </td>
         <td>
            <b>Максимум</b>
        </td>
    </tr>
    <?php
    foreach($experts as $expert){
        $settings_user = get_option("wif_4forum_blog_user_" . $expert->ID);
        if (!empty($settings_user)) {
            
            ?>
            <tr>
                <td style="border: 1px solid #000;"><input type="checkbox"
                                                           name="wif_4forum_blog_user_<?= $expert->ID ?>[paycheck]"
                                                           value="true" class="pay_check"></td>
                <td style="border: 1px solid #000;"><?= $expert->display_name ?></td>

                <td style="border: 1px solid #000;"><input type="text"
                                                           name="wif_4forum_blog_user_<?= $expert->ID ?>[blog_payment]"
                                                           value="<?= $settings_user['blog_payment'] ?>"></td>
                <td style="border: 1px solid #000;"><input type="text"
                                                           name="wif_4forum_blog_user_<?= $expert->ID ?>[image_blog_payment]"
                                                           value="<?= $settings_user['image_blog_payment'] ?>"></td>
                <td style="border: 1px solid #000;"><input type="text"
                                                           name="wif_4forum_blog_user_<?= $expert->ID ?>[max_payment]"
                                                           value="<?= $settings_user['max_payment'] ?>"></td>
            </tr>
            <?php
        }
    }
 ?>
    </tbody>
    </table>
<br>
    <table>
        <tbody>
        <tr>
            <td>
                <input type="submit" name="Submit"  class="button-primary" value="Сохранить" />
                <input type="hidden" name="save_payment_blog" value="1"/>
            </td>
         </tr>
        </tbody>
    </table>
    </form>
    <script type="text/javascript">
        jQuery(document).ready(function($){
            $('#pay_all').change(function() {
                var $this = $(this);
                if ($this.is(':checked')) {
                    $( ".pay_check" ).prop( "checked", true );
                } else {
                    $( ".pay_check" ).prop( "checked", false );
                }

            });
            $('#to_pay').click(function(){
                $('.pay_check:checked').parent().parent().find('.for_payment_r').val('0');
            });
        });
    </script>
<?php
}


function wif_4forum_payment_for_replyes_settings_tab(){
     $settings_default = get_option("wif_4forum_settings_default_payment");
    $args = array(
        'role' => 'expert',
        'orderby' => 'user_nicename',
        'order' => 'ASC'
    );
    $experts = get_users($args);
     //если пост команд
    if ($_POST['save_payment']){
        $options = $_POST['settings_default'];
        foreach ($settings_default as $k=>$v){
            $settings_default[$k] = $options[$k];
        }
        update_option('wif_4forum_settings_default_payment', $settings_default);
        foreach ($experts as $expert){
            $settings_user = get_option( "wif_4forum_settings_user_". $expert->ID);
            $options = $_POST['wif_4forum_settings_user_'.$expert->ID];
            $settings_user['payed_r'] = $settings_user['payed_r'] + $settings_user['for_payment_r'];
            foreach ($settings_user as $k=>$v){
                if ($k!='payed_r') {
                    $settings_user[$k] = $options[$k];
                }
            }
            update_option("wif_4forum_settings_user_". $expert->ID, $settings_user);
        }
    }

?>
    <h1>Настройки настройка оплаты</h1>
    <form method="post" action="<?php admin_url( 'themes.php?page=theme-settings' ); ?>">
    <table>
        <tbody>
            <tr>
                <td><h3>Настройка длины</h3></td>
            </tr>
                <tr>
                    <td>Длинна короткого комментария (символы):</td>
                    <td><input type="text" name="settings_default[short]" value="<?=$settings_default['short']?>"></td>
                </tr>
                <tr>
                    <td>Длинна среднего комментария (символы):</td>
                    <td><input type="text" name="settings_default[reply]" value="<?=$settings_default['reply']?>"></td>
                </tr>
                <tr>
                    <td>Длинна длинного комментария (символы):</td>
                    <td><input type="text" name="settings_default[long]" value="<?=$settings_default['long']?>"></td>
                </tr>
                <tr>
                    <td colspan="2"><h3>Настройка оплаты экспертов по умолчанию</h3></td>
                </tr>
                <tr>
                    <td>Цена короткого комментария (руб.):</td>
                    <td><input type="text" name="settings_default[short_payment]" value="<?=$settings_default['short_payment']?>"></td>
                </tr>
                <tr>
                    <td>Цена среднего комментария (руб.):</td>
                    <td><input type="text" name="settings_default[reply_payment]" value="<?=$settings_default['reply_payment']?>"></td>
                </tr>
                <tr>
                    <td>Цена длинного комментария (руб.):</td>
                    <td><input type="text" name="settings_default[long_payment]" value="<?=$settings_default['long_payment']?>"></td>
                </tr>
        </tbody>
    <table>
        <tbody>
            <tr>
                <td>
                    <input type="submit" name="Submit"  class="button-primary" value="Сохранить" />
                    <input type="hidden" name="save_payment" value="1"/>
                </td>
             </tr>
        </tbody>
    </table>

    </table>
        <br>
        <br>
        <h3>Настройка оплаты экспертов</h3>
    <table style="border-collapse: collapse; border: 1px solid #000; background: #ffffff;">
    <tbody>
    <tr>
        <td><input type="checkbox" id="pay_all" name="pay_all" value="true"></td>
        <td>
            <b>Логин</b>
        </td>
        <td>
             <b>Цена короткого</b>
        </td>
        <td>
         <b>Цена среднего</b>
        </td>
        <td>
            <b>Цена длинного</b>
        </td>
    </tr>
    <?php
    foreach($experts as $expert){
        $settings_user = get_option("wif_4forum_settings_user_" . $expert->ID);
        if (!empty($settings_user)) {
            ?>
            <tr>
                <td style="border: 1px solid #000;"><input type="checkbox"
                                                           name="wif_4forum_settings_user_<?= $expert->ID ?>[paycheck]"
                                                           value="true" class="pay_check"></td>
                <td style="border: 1px solid #000;"><?= $expert->display_name ?></td>
                <td style="border: 1px solid #000;"><input type="text"
                                                           name="wif_4forum_settings_user_<?= $expert->ID ?>[short_payment]"
                                                           value="<?= $settings_user['short_payment'] ?>"></td>
                <td style="border: 1px solid #000;"><input type="text"
                                                           name="wif_4forum_settings_user_<?= $expert->ID ?>[reply_payment]"
                                                           value="<?= $settings_user['reply_payment'] ?>"></td>
                <td style="border: 1px solid #000;"><input type="text"
                                                           name="wif_4forum_settings_user_<?= $expert->ID ?>[long_payment]"
                                                           value="<?= $settings_user['long_payment'] ?>"></td>
            </tr>
            <?php
        }
    }
 ?>
    </tbody>
    </table>
<br>
    <table>
        <tbody>
        <tr>
            <td>
                <input type="submit" name="Submit"  class="button-primary" value="Сохранить" />
                <input type="hidden" name="save_payment" value="1"/>
            </td>
         </tr>
        </tbody>
    </table>
    </form>
    <script type="text/javascript">
        jQuery(document).ready(function($){
            $('#pay_all').change(function() {
                var $this = $(this);
                if ($this.is(':checked')) {
                    $( ".pay_check" ).prop( "checked", true );
                } else {
                    $( ".pay_check" ).prop( "checked", false );
                }

            });
            $('#to_pay').click(function(){
                $('.pay_check:checked').parent().parent().find('.for_payment_r').val('0');
            });
        });
    </script>
<?php
}


?>