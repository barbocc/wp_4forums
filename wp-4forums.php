<?php

/*
Plugin Name: wp for forums
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: Плагин раскрутки форума.
Version: 0.0.1
Author: pogrom
Author URI: http://stolarchuk.ru
*/

//Хук начала синхронизации
add_shortcode( 'wif_4forum_4sync_page', 'wif_4forum_cron_sync' );

//Глобальные перменные для отправки пароля и логина на почту
define('WIF_4FORUM_DIR', plugin_dir_path(__FILE__));
define('WIF_4FORUM_URL', plugin_dir_url(__FILE__));

//Максимальная длинна заголовка темы форума
define('MAX_TITLE_SIZE', 40);

//Максимальная длинна
define('MAX_ANSWER_SIZE', 100);
require_once(WIF_4FORUM_DIR . 'includes/functions.php');
require_once(WIF_4FORUM_DIR . 'includes/parcing_functions.php');
require_once(WIF_4FORUM_DIR . 'includes/settings.php');


if (!class_exists('simple_html_dom')) {
	require_once(WIF_4FORUM_DIR . 'includes/simple_html_dom.php');
}

add_action( 'wp_insert_post', 'wif_4forum_new_rep', '101', 3);
add_action('init', 'wif_4forum_auto_login', 1);


//Если пользователь с рекламы создаём куки
add_action('init', 'wif_4forums_new_user_utm');
function wif_4forums_new_user_utm(){
	if ($_GET['new_user_utm']){
		setcookie ("user_from_adv_" . COOKIEHASH, true,time()+3600*24*7);
	}
}

//Если пользователь регистрируется выясняем пришёл ли он с рекламы с добавляем ему соовтесвующую метку
add_action('user_register', 'wif_4forums_registration_utm', 10, 1 );
function wif_4forums_registration_utm($user_id){
	if ( isset( $_COOKIE['user_from_adv_' . COOKIEHASH] ) ) {
		update_user_meta( $user_id, 'user_from_adv', true );
	}
}

add_filter('bbp_new_topic_pre_insert','wif_4forum_not_reged_question');
function wif_4forum_not_reged_question($topic_data = array()){
	
	if ($topic_data['post_author'] == 0 && $_COOKIE['comment_author_email_' . COOKIEHASH]){
		$topic_data['post_status'] = 'pending';
		$user_email= $_COOKIE['comment_author_email_' . COOKIEHASH];
		$name = $_COOKIE['comment_author_' . COOKIEHASH];
		$author = wif_4forum_get_author($user_email, $name);
		$topic_data['post_author'] = $author->ID;
		if ($author->passwd_source){
			update_user_meta( $author->ID, 'passwd_source', $author->passwd_source);
		}
	}
	return $topic_data;
}


function wif_4forums_redirect_anon($redirect_url = null, $redirect_to = null, $topic_id = null){
	//
	$topic = get_post($topic_id);
	
	if ($topic->post_status == 'pending'){
		//Страница с просьюой подтвердить вопрос
		$redirect_url = 'http://obustroeno.com/spasibo-za-vash-vopros';
		
		$topic_link = $topic->guid;
		$author = get_user_by('ID', $topic->post_author);
		$user_email = $author->user_email;
		$author->passwd_source = get_user_meta($topic->post_author,'passwd_source',true);
		$subject = '';
		$message = $topic->post_content;
		//Отправка письма
		if ($author->passwd_source) {
			$letter = '<p>Здравствуйте, мы получили ваш вопрос:</p>
		<p style="background: #f0d500; padding-left: 30px; padding-top: 10px; padding-bottom: 10px; font-weight:bold;">'.$message.'</p>
		<p>Наши эксперты отвечают только на вопросы, опубликованные на нашем форуме.
		 Для того, чтобы автоматически опубликовать вопрос на форуме, перейдите, пожалуйста, по ссылке
		  <a href="'.$topic_link.'?wif_login='.$author->user_login.'&agree=yes&wif_pass='.$author->passwd_source.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=reg_letter&utm_content='.$author->user_email.'" style="color:#0171a1;">'.$topic_link .'?wif_login='.$author->user_login.'&agree=yes&wif_pass='.$author->passwd_source.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=reg_letter&utm_content='.$author->user_email.'
		   </a>.
		</p>
		<p>Обычно время ответа экспертов от 5 минут до 2 суток (это полностью бесплатно и делается для развития сообщества и форума). Мы ничего не продаём и не оказываем никаких услуг.</p>
		<!--Для удобства общения, мы создали Вам аккаунт:</p></div><footer style="background: #e6e6e6;height: 80px;margin-top: 20px; font-family: \'Helvetica Neue\', Arial, Helvetica, \'Nimbus Sans L\', sans-serif;font-size: 10pt; margin: 0 auto; text-align: center">
		    <table cellpadding="0" cellspacing="0" style="border: solid 1px #e6e6e6;">
		        <tr>
		            <td style="border: solid 1px #e6e6e6; padding: 4px;">Логин:</td>
		            <td style="border: solid 1px #e6e6e6; padding: 4px;">'.$author->user_login.'</td>
		        </tr>
		        <tr>
		            <td style="border: solid 1px #e6e6e6; padding: 4px;">Пароль:</td>
		            <td style="border: solid 1px #e6e6e6; padding: 4px;">'.$author->passwd_source.'</td>
		        </tr>
		    </table>-->';
			$footer = '<!--<p>Хотите получить ответ быстрее?</p>
		<p style="font-weight: bold;">Отпишитесь о себе в теме для новичков форума: <a href="http://obustroeno.com/topic/49096?wif_login='.$author->user_login.'&agree=yes&wif_pass='.$author->passwd_source.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=reg_letter_znak&utm_content='.$author->user_email.'">http://obustroeno.com/topic/49096</a>. Так Вас быстрее заметят и Вы раньше получите ответ на вопрос.</p>
		   -->
		        <p>В случае проблем с регистрацией отпишитесь по адресу: support@obustroeno.com</p>
		        <p>Если вы не хотите публиковать этот вопрос, просто проигнорируйте письмо, но тогда эксперты не смогут вам ответить.</p>
		        <a href="'.$topic_link.'?wif_login='.$author->user_login.'&agree=yes&wif_pass='.$author->passwd_source.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=reg_letter&utm_content='.$author->user_email.'" style="position:relative; top: -23;font-weight: bold; width: 60%; background: #0171a1; display: inline-block; height: 40px; color: #fff; font-size: 14pt; position: relative; top: -10px; padding-top: 10px;padding-left: 10px;padding-right: 10px;">
		            Подтвердить публикацию и  получить ответы экспертов
		        </a><br>
		        <span>С уважением, сообщество <a href="http://obustroeno.com?wif_login='.$author->user_login.'&agree=yes&wif_pass='.$author->passwd_source.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=reg_letter&utm_content='.$author->user_email.'" style="color:#0171a1;">Obustroeno.com</a></span>
		    </footer>';
			wif_4forum_send_mail($user_email, $letter.$footer, $subject);
			delete_user_meta($topic->post_author, 'passwd_source');
		}
		else {
			
		}
		
	}
	return $redirect_url;
}

/***
 * Оплата за новую публикацию
 ***/
add_action( 'publish_post', 'wif_4forum_payment_for_new_post', 10, 2 );
function wif_4forum_payment_for_new_post( $post_id, $post ) {
	
	// If this is a revision or update do nothing
	if ($post->post_type == 'post') {
		$author = get_user_by( 'ID', $post->post_author );

		if (in_array('expert',$author->roles)){
			$settings_blog_payment = get_option( "wif_4forum_blog_user_". $author->ID);
			
			$post_money = $settings_blog_payment['blog_payment'];
			$image_money = $settings_blog_payment['image_blog_payment'];
			$maximum = $settings_blog_payment['max_payment'];
			
			//количество картинок
			$attachments = get_posts ( array(
				'numberposts' => -1,
				'post_type' => 'attachment',
				'post_mime_type' => 'image',
				'post_parent' => $post_id,
				'post_status' => 'inherit',
			) );
			
			//галлереи тоже
			
			$money = count( $attachments ) * $image_money + $post_money;
			if ( $money > $maximum ) {
				$money = $maximum;
			}
			update_post_meta( $post_id,'attached_pictures_count',count( $attachments ));
			update_post_meta( $post_id, 'payment', $money);
		}
	}
}

add_filter('bbp_new_topic_redirect_to', 'wif_4forums_redirect_anon', 10, 3);
function wif_4forum_auto_login(){
	
	if ( $_GET['agree'] === 'yes' ) {
		$agree = true;
	}
	
	if (!is_user_logged_in()&&$_GET['wif_login']&&$_GET['wif_pass']){
		$creds = array(
			'user_login'    => $_GET['wif_login'],
			'user_password' => $_GET['wif_pass'],
			'remember'      => true,
		);
		$user = wp_signon( $creds, false );
		
		//если пользователь согласен
		wp_set_current_user($user->ID);
		
		$user =  wp_get_current_user();
		
		if ($agree && $user->ID) {
			//Извлекаю все вопросы
			$args = array(
				'author' => $user->ID,
				'post_type' => 'topic',
				'post_status' => 'pending'
			);
			$posts = get_posts($args);
			
			//И публикую
			foreach ($posts as $post) {
				$args = array(
					'ID' => $post->ID,
					'post_status' => 'publish'
				);
				
				wp_update_post( $args );
			}
		}
		
	}
}


function wif_4forum_synс_discus(){
	//Извлечение настроек
	$settings_discus = get_option('wif_4forum_settings_discus');
	
	wif_4forum_errs('Начали синхронизацию ответов...', ' ...');
	
	if (empty($settings_discus)){
		return null;
	}
	
	if ($settings_discus['imap_path']=='' || $settings_discus['email']=='' ||
	    $settings_discus['pass']==''){
		return null;
	}
	
	$email = $settings_discus['email'];
	$pass = $settings_discus['pass'];
	$last = $settings_discus['last_one'];
	$moderated = $settings_discus['imap_path'];
	$processed = $settings_discus['imap_path_to'];
	
	$answers = wif_4forum_get_content_of_letter($email, $pass, $moderated, $processed, $last);
	
	//парсинг ответов
	foreach($answers as $answer){
		
		$from = $answer['from'];
		if ($from['name']) $user_name = $from['name'];
		
		//Парсим ответ
		if(mb_strpos($from['name'],'venyoo')||mb_strpos($from['email'],'venyoo')){
			$dict = wif_4forum_venyoo_parsing($answer['text']);
		}
		//Иначе если вопрос из формы
		else{
			$dict = wif_4forum_form_parsing($answer['text']);
		}
		if($dict!==null){
			
			$message = $dict['message'];
			$user_email = $dict['email'];
		}
		else continue;
		
		if($dict['from']) $user_name = $dict['from'];
		elseif(mb_strpos($from['name'],'venyoo')||mb_strpos($from['email'],'venyoo')) $user_name = $dict['from'];
		else $user_name = $from['name'];
		
		$image_files = $answer['files'];
		
		//Проверяем миниальный размер и если нет картинок. пропускаем сообщение мимо
		//if (mb_strlen($message)<$settings_discus['min_length'] && !$image_files) continue;
		
		//Извлекаем данные автора
		$author = wif_4forum_get_author($user_email, $user_name);
		
		$topic = get_post($settings_discus['discus']);
		$forum_id = $topic->post_parent;
		
		
		//Размещаем ответ от имени пользователя
		$reply_data = array(
			'post_parent'    => $settings_discus['discus'], // topic ID
			//'post_status'    => bbp_get_public_status_id(),
			//'post_type'      => bbp_get_reply_post_type(),
			'post_author'    => $author->ID,
			'post_content'   => $message,
			'post_title'     => '',
			'menu_order'     => 0,
			'comment_status' => 'closed'
		);
		
		$reply_id = bbp_insert_reply($reply_data, array('forum_id' => $forum_id,'topic_id' => $settings_discus['discus']));
		update_post_meta($reply_id, 'generated', date("Y-m-d H:i:s"));
		
		echo "<br>".$settings_discus['discus']."<br>";
		
		//Если есть прикрепляем файлы
		if(is_array($image_files)) foreach($image_files as $file){
			$url_arr = explode('/',$file['tmp_name']);
			$url = 'http://'. $url_arr[4] . '/'. $url_arr[6] . '/'. $url_arr[7] . '/'.  $url_arr[8]  . '/'.  $url_arr[9] . '/' ;
			
			$attachment = array(
				'guid'           => $url . $file['hashed_filename'],
				'post_mime_type' => $file['type'],
				'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $file['hashed_filename'] ) ),
				'post_content'   => '',
				'post_status'    => 'inherit'
			);
			$attach_id = wp_insert_attachment( $attachment, $file['tmp_name'], $reply_id);
			$attach_data = wp_generate_attachment_metadata( $attach_id, $file['tmp_name']);
			wp_update_attachment_metadata($attach_id, $attach_data);
			update_post_meta($attach_id, '_bbp_attachment', '1');
			
		}
		
		$topic_link = '';
		
		$topic_link = get_permalink($settings_discus['discus']);
		$topic_id = $settings_discus['discus'];
		$topic = get_post($topic_id);
		$subject = $topic->post_title;
		
		//Если стоит галочка не отправлять письма*/
		if ($settings_discus['stop_mail']) continue;
		if ($author->passwd_source){
			$letter = '<p>Здравствуйте, благодарим вас за участие в проекте "Народный контроль". Мы получили ваш отзыв:</p>
<p style="background: #f0d500; padding-left: 30px; padding-top: 10px; padding-bottom: 10px; font-weight:bold;">'.$message.'</p>
<p>Мы приняли решение опубликовать его на сайте нашего проекта <a href="'.$topic_link.'?wif_login='.$author->user_login.'&wif_pass='.$author->passwd_source.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=reg_letter_topic&utm_content='.$author->user_email.'" style="color:#0171a1;">'.$topic_link.'</a>.</p>
<p>На случай если Вы захотите имзменить или дополнить отзыв, мы создали аккаунт:</p>
    <table cellpadding="0" cellspacing="0" style="border: solid 1px #e6e6e6;">
        <tr>
            <td style="border: solid 1px #e6e6e6; padding: 4px;">Логин:</td>
            <td style="border: solid 1px #e6e6e6; padding: 4px;">'.$author->user_login.'</td>
        </tr>
        <tr>
            <td style="border: solid 1px #e6e6e6; padding: 4px;">Пароль:</td>
            <td style="border: solid 1px #e6e6e6; padding: 4px;">'.$author->passwd_source.'</td>
        </tr>
    </table>
        <p style="font-weight:bold;">Начните вести свой блог ремонта на проекте: <a href="http://obustroeno.com/new-record/?wif_login='.$author->user_login.'&wif_pass='.$author->passwd_source.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=reg_letter_topic_blog&utm_content='.$author->user_email.'" style="color:#0171a1;">http://obustroeno.com/new-record/</a></p>
                <p>Просмотреть Ваш отзыв и отзывы других людей можете по ссылке <a href="'.$topic_link.'?wif_login='.$author->user_login.'&wif_pass='.$author->passwd_source.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=reg_letter_topic&utm_content='.$author->user_email.'" style="color:#0171a1;">'.$topic_link.'</a></p>
                <p>В случае проблем с регистрацией или если Вы не согласны с публикацией Вашего отзыва, отпишитесь об этом по адресу: support@obustroeno.com.</p>';
			$footer = '<footer style="background: #e6e6e6;height: 80px;margin-top: 20px; font-family: \'Helvetica Neue\', Arial, Helvetica, \'Nimbus Sans L\', sans-serif;font-size: 10pt; margin: 0 auto; text-align: center">
        <a href="'.$topic_link.'?wif_login='.$author->user_login.'&wif_pass='.$author->passwd_source.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=reg_letter_topic&utm_content='.$author->user_email.'" style="position:relative; top: -23;font-weight: bold; width: 60%; background: #0171a1; display: inline-block; height: 40px; color: #fff; font-size: 14pt; position: relative; top: -10px; padding-top: 10px;padding-left: 10px;padding-right: 10px;">
            Перейти к отзыву
        </a><br>
        <span>С уважением, сообщество <a href="http://obustroeno.com?wif_login='.$author->user_login.'&wif_pass='.$author->passwd_source.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=reg_letter&utm_content='.$author->user_email.'" style="color:#0171a1;">Obustroeno.com</a></span>
    </footer>';
			
			
			
			wif_4forum_send_mail($user_email, $letter.$footer, $subject);
		}
		//Письмо если человек уже задавал вопрос
		
		else{
			$letter = '<p>Здравствуйте, благодарим вас за участие в проекте "Народный контроль". Мы получили ваш отзыв:</p>
<p style="background: #f0d500;  padding-left: 30px; padding-top: 10px; padding-bottom: 10px;">'.$message.'</p>
<p>Мы приняли решение опубликовать его на сайте нашего проекта <a href="'.$topic_link.'?utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=question_letter_topic&utm_content='.$author->user_email.'" style="color:#0171a1;">'.$topic_link.'</a>.</p>
<p>Если Вы не согласны с публикацией Вашего отзыва, отпишитесь об этом по адресу: support@obustroeno.com.</p>
    <p style="font-weight: bold;">Начните вести свой блог ремонта на проекте: <a href="http://obustroeno.com/new-record/?utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=question_letter_topic_blog&utm_content='.$author->user_email.'" style="color:#0171a1;">http://obustroeno.com/new-record/</a>.
    </p>
';
			$footer = '<footer style="background: #e6e6e6;height: 80px;margin-top: 20px; font-family: \'Helvetica Neue\', Arial, Helvetica, \'Nimbus Sans L\', sans-serif;font-size: 10pt; margin: 0 auto; text-align: center">
        <a href="'.$topic_link.'?utm_source=email&utm_medium=email&utm_term=link3&utm_campaign=question_letter_topic&utm_content='.$author->user_email.'" style="position:relative; top: -23;font-weight: bold; width: 60%; background: #0171a1; display: inline-block; height: 40px; color: #fff; font-size: 14pt; position: relative; top: -10px; padding-top: 10px;padding-left: 10px;padding-right: 10px;">
            Перейти к отзыву
        </a><br>
        <span>
        С уважением, сообщество <a href="http://obustroeno.com?utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=question_letter_topic&utm_content='.$author->user_email.'" style="color:#0171a1;">Obustroeno.com</a></span>
    </footer>';
			$stop_send_letters_flag = get_user_meta($author->ID, 'stop_send_letters', true);
			if (!$stop_send_letters_flag) {
				wif_4forum_send_mail($user_email, $letter . $footer, $subject);
			}
		}
	}
}

function wif_4forum_new_rep($post_id, $post, $update){
	//Проверяем является ли ответ новым, роль пользователя, тип поста
	
	global $current_user;
	$post_type = get_post_type($post_id);
	$post = get_post($post_id);
	
	//В этом блоке увысчитываем деньги нашему автору
	if (!$update&&$current_user->roles[0]=='expert'&&$post_type=='reply') {
		//Извлекаем последний пост темы
		$args = array(
			'post_parent' => $post->post_parent,
			'post_status' => 'publish',
			'post_type' => 'reply',
			'numberposts' => -1
			//'post_author'=> $post->post_author
		);
		
		$settings_user = get_option("wif_4forum_settings_user_" . $post->post_author);
		$settings_payment = get_option("wif_4forum_settings_default_payment");
		
		$replies = get_posts($args);
		
		$previous_payment = 0;
		$count = mb_strlen($post->post_content, 'UTF-8');
		
		//Орпеделяем явлется ли предидущий коментарий от того же автора
		//if ($replies[0]->post_author == $post->post_author) {
		
		foreach ($replies as $reply){
			
			if ( $reply->post_author !== $post->post_author ){
				break;
			}
			
			$reply_pay = get_post_meta($reply->ID, 'reply_payment', true);
			
			if ( $reply->post_author === $post->post_author && $reply_pay > 0){
				$count = $count + mb_strlen($reply->post_content, 'UTF-8');
				$reply_id = $reply->ID;
				break;
			}
			elseif ( $reply->post_author === $post->post_author && $reply_pay == 0 ){
				$count = $count + mb_strlen($reply->post_content, 'UTF-8');
			}
		}
		
		if (!empty(get_post_meta($replies[0]->ID, 'reply_payment'))) {
			$previous_payment = get_post_meta($replies[0]->ID, 'reply_payment', true);
		}
		
		if ( (int)$count < (int)$settings_payment['short'] && (int)$count > 0 ) {
			$payment = $settings_user['short_payment'];
			$category = 'short';
		} else if ( (int)$count < (int)$settings_payment['reply'] ) {
			$payment = $settings_user['reply_payment'];
			$category = 'reply';
		} else {
			$payment = $settings_user['long_payment'];
			$category = 'long';
		}
		
		if ( isset( $reply_id ) ) {
			update_post_meta( $reply_id, 'reply_cat', $category );
			update_post_meta( $reply_id, 'reply_length', $count );
			update_post_meta( $reply_id, 'reply_payment', $payment );
			
			update_post_meta( $post_id, 'reply_cat', 'repeat' );
			update_post_meta( $post_id, 'reply_length', 0 );
			update_post_meta( $post_id, 'reply_payment', 0 );
		}
		
		else {
			update_post_meta( $post_id, 'reply_cat', $category );
			update_post_meta( $post_id, 'reply_length', $count );
			update_post_meta( $post_id, 'reply_payment', $payment );
		}
		
	}
	
	
	wif_4forum_new_answer($post);
}

// $CurrentAnswerPost
function wif_4forum_new_answer($post){ //send
	
	$parent_id = wp_get_post_parent_id($post->ID);
	$parent = get_post($parent_id); // ParentTopicOfPost  ParentTopic
	$author_parent = get_userdata($parent->post_author);
	$stop_send_letters_flag = get_user_meta( $parent->post_author, 'stop_send_letters', true);
	
	if (get_post_type($parent)=='topic'&& get_post_type($post)=='reply' && $post->post_author!=='603'){
		
		
		$author_post = get_userdata($post->post_author);
		$answer = '';
		$text = wp_strip_all_tags($post->post_content);
		$length = mb_strlen($text, 'UTF-8');
		//$answer = $length;
		
		if ($length > MAX_ANSWER_SIZE){
			$answer = mb_substr($text,0 , MAX_ANSWER_SIZE).'...';
		}
		else {
			$answer = $text;
		}
		
		$answer = preg_replace('#\[[^\]]+\]#', '', $answer);
		
		$letter = '<p>Здравствуйте '. $author_parent->user_login .', на Ваш вопрос был получен ответ от пользователя:</p>
<p style="background: #f0d500; padding-left: 30px; padding-top: 10px; padding-bottom: 10px;">'.$answer.' <a href="'.get_permalink($parent).'?'.'?utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $parent->ID).'&utm_campaign=answer_letter&utm_content='.$author_parent->user_email.'">(Читать далее)</a></p>
<p>Для просмотра полного ответа, перейдите пожалуйста по <a href="'.get_permalink($parent).'?'.'?utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $parent->ID).'&utm_campaign=answer_letter&utm_content='.$author_parent->user_email.'" style="color:#0171a1;">ссылке</a>.</p>
<p>Данный ответ был получен на заданный ранее вопрос:</p>
<p style="background: #ededed; padding-left: 30px; padding-top: 10px; padding-bottom: 10px;">'.$parent->post_content.'</p>
                <p>Начните вести свой блог ремонта на проекте: <a href="http://obustroeno.com/new-record/?utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=answer_letter_blog&utm_content='.$author_parent->user_email.'" style="color:#0171a1;">http://obustroeno.com/new-record/</a></p>
';
		$subject = 'На ваш вопрос: "'. $parent->post_title .'" - получен ответ';
		//$author->user_email
		$footer = '</div><footer style="background: #e6e6e6;height: 80px;margin-top: 20px; font-family: \'Helvetica Neue\', Arial, Helvetica, \'Nimbus Sans L\', sans-serif;font-size: 10pt; margin: 0 auto; text-align: center">
        <a href="'.get_permalink($parent).'?'.'?utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y',  $parent->ID).'&utm_campaign=answer_letter&utm_content='.$author_parent->user_email.'" style="position: relative; top: -23; font-weight: bold; width: 60%; background: #0171a1; display: inline-block; height: 40px; color: #fff; font-size: 14pt; position: relative; top: -10px; padding-top: 10px;padding-left: 10px;padding-right: 10px;">
            Перейти к ответу
        </a><br>
        <p style="font-weight: bold;">Если вы всё ещё отписались в теме знакомства <a href="http://obustroeno.com/topic/49096?utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $parent->ID).'&utm_campaign=answer_letter_znak&utm_content='.$author_parent->user_email.'">http://obustroeno.com/topic/49096</a>, отпишитесь, пожалуйста. Нам и экспертам будет приятно :)</p>
        <span>С уважением сообщество профессионалов <a href="http://obustoeno.com?utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $parent->ID).'&utm_campaign=answer_letter&utm_content='.$author_parent->user_email.'" style="color:#0171a1;">Obustroeno.com</a></span>
    </footer>';
		
		if ($parent->post_author !== $post->post_author){
			
			if (!$stop_send_letters_flag) {
				wif_4forum_send_mail($author_parent->user_email, $letter . $footer, $subject);
			}
		}
		else{
			
			wif_4forum_send_letters2experts($parent, $answer);
		}
	}
}

function wif_4forum_send_letters2experts($parent, $answer){ // wif_4forum_send_letters_to_experts

//Извлекаем все посты типа ответ и темы parent
	$args =  array(
		'post_parent' => $parent->ID,
		'post_status' => 'publish',
		'post_type' => 'reply',
	);
	
	$posts  = get_posts($args);
	$author_parent = get_userdata($parent->post_author);
	/*
		$text = wp_strip_all_tags($amswer_post->post_content);
		$length = mb_strlen($text, 'UTF-8');
		if ($length > MAX_ANSWER_SIZE){
			$answer = mb_substr($text, 0, MAX_ANSWER_SIZE) . '...';
		}
		else {
			$answer = $text;
		}
	*/
	$experts = array();
	foreach($posts as $post){
		
		$author_post = get_userdata($post->post_author);
		if (($parent->post_author !== $post->post_author) && ($author_post->roles[0] === 'expert')&&(!$experts[$post->post_author])){
			$experts[$post->post_author] = true;
			$letter =  '<p>Здравствуйте уважаемый эксперт, '. $author_post->user_login .', на Ваш ответ был получен комментарий от пользователя '.$author_parent->user_login.':</p>
<p style="background: #f0d500; padding-left: 30px; padding-top: 10px; padding-bottom: 10px;">'.$answer.' <a href="'.get_permalink($parent).'?'.'?utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $parent->ID).'&utm_campaign=expert_letter&utm_content='.$author_post->user_email.'">(Читать далее)</a></p>
<p>Для просмотра полного комментария, перейдите пожалуйста по <a href="'.get_permalink($parent).'?'.'?utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $parent->ID).'&utm_campaign=expert_letter&utm_content='.$author_post->user_email.'" style="color:#0171a1;">ссылке</a>.</p>
<p>Данный ответ был получен на заданный ранее вопрос пользователя '.$author_parent->user_login.':</p>
<p style="background: #ededed; padding-left: 30px; padding-top: 10px; padding-bottom: 10px;">'.$parent->post_content.'</p>
                <p>Начните вести свой блог ремонта на проекте: <a href="http://obustroeno.com/new-record/?utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=expert_letter_blog&utm_content='.$author->user_email.'" style="color:#0171a1;">http://obustroeno.com/new-record/</a></p>
';
			$subject = 'На вопрос: "'. $parent->post_title .'" - получен комментарий автора';
			$footer = '</div><footer style="background: #e6e6e6;height: 80px;margin-top: 20px; font-family: \'Helvetica Neue\', Arial, Helvetica, \'Nimbus Sans L\', sans-serif;font-size: 10pt; margin: 0 auto; text-align: center">
        <a href="'.get_permalink($parent).'?'.'?utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $parent->ID).'&utm_campaign=expert_letter&utm_content='.$author_post->user_email.'" style="position: relative; top: -23; font-weight: bold; width: 60%; background: #0171a1; display: inline-block; height: 40px; color: #fff; font-size: 14pt; position: relative; top: -10px; padding-top: 10px;padding-left: 10px;padding-right: 10px;">
            Перейти к ответу
        </a><br>
        <span>С уважением сообщество профессионалов <a href="http://obustoeno.com?utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $parent->ID).'&utm_campaign=expert_letter&utm_content='.$author_post->user_email.'" style="color:#0171a1;">Obustroeno.com</a></span>
    </footer>';
			$stop_send_letters_flag = get_user_meta( $author_post->ID, 'stop_send_letters', true);
			if (!$stop_send_letters_flag) {
				wif_4forum_send_mail($author_post->user_email, $letter . $footer, $subject);
			}
		}
	}
}

function wif_4forum_cron_sync(){
	$settings_general = get_option( "wif_4forum_settings_general" );
	$settings_reply = get_option( "wif_4forum_settings_reply" );
	$settings_discus = get_option( "wif_4forum_settings_discus" );
	//Парсинг вопрос заданных пользователями
	if (!$settings_general['stop']){//Если не стоит галочка "прервать синхронизацию"
		wif_4forum_sync();
	}
	
	
	//Парсинг ответов на почтовый ящик
	if ($settings_reply['enable_responses']) {//Если стоит галочка "активировать синхронизацию"
		wif_4forum_synс_replies();
	}
	
	//парсинг дискуссии
	if ($settings_discus['enable_responses'])  wif_4forum_synс_discus(); //Если стоит галочка "активировать синхронизацию"
	
	
}


function wif_4forum_errs($error, $solution){
	$errors_log = get_option("wif_4forum_errors_log");
	$data = date("d/m/Y");
	$line = $data . '    ' . $error . '    ' . $solution . PHP_EOL;
	//var_dump($line);
	$errors_log = $line . $errors_log;
	update_option("wif_4forum_errors_log", $errors_log);
}

//Функция синхронизации ответов от пользователей
function wif_4forum_synс_replies(){
	wif_4forum_errs('Начали синхронизацию ответов...', ' ...');
	
	$settings_reply = get_option("wif_4forum_settings_reply");
	if (empty($settings_reply)){
		return null;
	}
	
	if ($settings_reply['imap_path']=='' || $settings_reply['email']=='' ||
	    $settings_reply['pass']==''){
		return null;
	}
	
	$email = $settings_reply['email'];
	$pass = $settings_reply['pass'];
	$last = $settings_reply['last_one'];
	$path = $settings_reply['imap_path'];
	$processed = $settings_reply['imap_path_to'];
	
	$answers = wif_4forum_get_content_of_letter($email, $pass, $path, $processed,$last);
	//var_dump($answers);
	//var_dump(wp_mail('barbocc@gmail.com','test','test'));
	//парсинг ответов
	foreach($answers as $answer){
		
		$from = $answer['from'];
		$image_files =  $answer['files'];
		//echo $from['email'];
		if ($from['name']) $user_name = $from['name'];
		$author = wif_4forum_get_author($from['email'], $user_name);
		
		//Парсим ответ
		$text_and_topicid =  wif_4forum_parse_reply($answer['text'], $from['email']);
		
		//var_dump($text_and_topicid);
		
		//Проверям минимальную длинну текста и наличие файлов
		
		if (mb_strlen($text_and_topicid['text'])<$settings_reply['min_length'] && !$image_files) continue;
		elseif(mb_strlen($text_and_topicid['text'])< 5) {
			$text_and_topicid['text'] = 'Приклыдываю файлы';
		}
		
		$topic = get_post($text_and_topicid['topic_id']);
		$forum_id = $topic->post_parent;
		//echo $forum_id;
		
		//Размещаем ответ от имени пользователя
		$reply_data = array(
			'post_parent'    =>$text_and_topicid['topic_id'], // topic ID
			//'post_status'    => bbp_get_public_status_id(),
			//'post_type'      => bbp_get_reply_post_type(),
			'post_author'    => $author->ID,
			'post_content'   => $text_and_topicid['text'],
			'post_title'     => '',
			'menu_order'     => 0,
			'comment_status' => 'closed'
		);
		
		$reply_id = bbp_insert_reply($reply_data, array('forum_id' => $forum_id,'topic_id' => $text_and_topicid['topic_id']));
		update_post_meta($reply_id, 'generated', date("Y-m-d H:i:s"));
		
		echo "<br>".$text_and_topicid['topic_id']."<br>";
		
		//Если есть прикрепляем файлы
		if(is_array($image_files)) foreach($image_files as $file){
			$url_arr = explode('/',$file['tmp_name']);
			$url = 'http://'. $url_arr[4] . '/'. $url_arr[6] . '/'. $url_arr[7] . '/'.  $url_arr[8]  . '/'.  $url_arr[9] . '/' ;
			
			$attachment = array(
				'guid'           => $url . $file['hashed_filename'],
				'post_mime_type' => $file['type'],
				'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $file['hashed_filename'] ) ),
				'post_content'   => '',
				'post_status'    => 'inherit'
			);
			$attach_id = wp_insert_attachment( $attachment, $file['tmp_name'], $reply_id);
			$attach_data = wp_generate_attachment_metadata( $attach_id, $file['tmp_name']);
			wp_update_attachment_metadata($attach_id, $attach_data);
			update_post_meta($attach_id, '_bbp_attachment', '1');
			
			//$reply = get_post($reply_id);
			//$reply->post_content = $reply->post_content . '<img src="'.$attachment['guid'].'" />';
			//wp_update_post($reply);
			
		}
		//Если не стоит галочки не отправлять письма расслыаем их экспертам*/
		if (!$settings_reply['stop_mail']) wif_4forum_send_letters2experts($text_and_topicid['topic_id'], $text_and_topicid['text']);
	}
	
}

//Функция прикрепляет изображения красиво через плагин GD attach
function wif_4forum_attach_imgs($post_id, $images){
	return null;
}

function wif_4forum_parse_reply($reply, $author_email, $stop_words = null){
	//парсим дерево
	//находим в конце "obustroeno.com".
	$reply_arr = mb_explode('"obustroeno.com"',$reply);
	if(!is_array($reply_arr ) || count($reply_arr ) < 2) {
		$reply_arr = mb_explode('no-reply@obustroeno.com',$reply);
		//Если из mail.ru
		if (count($reply_arr) > 1) $reply_arr[0] = mb_explode('Отправлено из Mail.Ru', $reply_arr[0])[0];
	}
	
	if(!is_array($reply_arr ) || count($reply_arr) < 2) return null;
	
	$quote_text = $reply_arr[1];
	
	//очищаем сообщение
	
	//убираем из сообщения дату в формате день.месяц.год 00.00.0000
	$pattern = "/\d{2}\.\d{2}.\d{4}/";
	$reply_arr[0] = preg_split($pattern , $reply_arr[0])[0];
	
	//убираем из сообщения даты в формате Понедельник, 00
	$pattern = "/(?:понедельник)|(?:Понедельник)|(?:Вторник)|(?:вторник)|(?:Среда)|(?:среда)|(?:Четверг)|(?:четверг)|(?:Пятница)|(?:пятница)|(?:суббота)|(?:Суббота)|(?:воскресение)(?:Воскресение)\,\s\d{1,2}/";
	$reply_arr[0] = preg_split($pattern , $reply_arr[0])[0];
	// mb_substr($string, 0, -1);
	if (mb_substr($reply_arr[0], -1)=='>') $reply_arr[0] = mb_substr($reply_arr[0], 0, -1);
	
	$reply_text = $reply_arr[0];
	
	//извлекаем ссылку
	//извлекаем топик по ссылке в цитировании, если ссылки нет, берём последний топик этого автора.
	//echo mb_strlen($reply_text).'<br>';
	
	$user = get_user_by('email', $author_email);
	//var_dump($user);
	echo $user->ID . '<br>';
	if (!$user) return null;
	
	$args = array(
		'author' => $user->ID,
		'post_status' => 'publish',
		'post_type' => 'topic',
		'numberposts' => 1
		//'posts_per_page' => 1
	);
	
	$last_posts = get_posts($args);
	$text_topic_id['topic_id'] = $last_posts[0]->ID;
	$text_topic_id['text'] = $reply_text;
	
	
	return $text_topic_id;
	//return null; //возвращаем текст письма и id топика, к которому привязана
}

//Функция извлечения содержимого из письма
function wif_4forum_get_content_of_letter($username, $pass, $imapPath, $to, $last){
	
	$messages = null;//массив в котором будут харниться все сообщения в надлежащей структуре
	$inbox = imap_open($imapPath, $username, $pass);
	
	//$mailboxes = imap_list($inbox, $from, '*');
	//var_dump($mailboxes);
	
	if (!$inbox){
		wif_4forum_errs('Ошибка открытия imap', 'проверьте включён ли модуль imap для php на сервере,
     верно ли введены настройки почтового ящика в плагине');
		return;
	}
	
	// Возможные фильтры писем - чтобы не гуглить каждый раз. ALL значит, что мы  выбираем все письма
	/* ALL - return all messages matching the rest of the criteria
	 ANSWERED - match messages with the \\ANSWERED flag set
	 BCC "string" - match messages with "string" in the Bcc: field
	 BEFORE "date" - match messages with Date: before "date"
	 BODY "string" - match messages with "string" in the body of the message
	 CC "string" - match messages with "string" in the Cc: field
	 DELETED - match deleted messages
	 FLAGGED - match messages with the \\FLAGGED (sometimes referred to as Important or Urgent) flag set
	 FROM "string" - match messages with "string" in the From: field
	 KEYWORD "string" - match messages with "string" as a keyword
	 NEW - match new messages
	 OLD - match old messages
	 ON "date" - match messages with Date: matching "date"
	 RECENT - match messages with the \\RECENT flag set
	 SEEN - match messages that have been read (the \\SEEN flag is set)
	 SINCE "date" - match messages with Date: after "date"
	 SUBJECT "string" - match messages with "string" in the Subject:
	 TEXT "string" - match messages with text "string"
	 TO "string" - match messages with "string" in the To:
	 UNANSWERED - match messages that have not been answered
	 UNDELETED - match messages that are not deleted
	 UNFLAGGED - match messages that are not flagged
	 UNKEYWORD "string" - match messages that do not have the keyword "string"
	 UNSEEN - match messages which have not been read yet*/
	
	
	// Массив, содержащий все письма. Если письмо было не в INBOX, а в другой подпапке - оно сюда не попадет
	$emails = imap_search($inbox,'ALL');
	
	foreach($emails as $mail) {
		
		if ($last) {
			$mail = end($emails);
		}
		$headerInfo = imap_headerinfo($inbox, $mail);
		$personal = $headerInfo->from[0]->personal;
		$from['name'] = ($personal) ? mb_decode_mimeheader($headerInfo->from[0]->personal): null;
		//$from['email'] = ($headerInfo->fromaddress) ? mb_decode_mimeheader($headerInfo->fromaddress): null;
		$from['email'] = $headerInfo->from[0]->mailbox . "@" . $headerInfo->from[0]->host;
		$subject = $headerInfo->Subject;
		
		$emailStructure = imap_fetchstructure($inbox, $mail);
		$text= ''; //Храниться текст сообщения
		$image_files = null; //массив в котором хранятся все изображения, в дальнейшем надо будет доработать до любого файла
		//Магический указатель для обработки изображений
		$i = 2;
		//var_dump($emailStructure);
		if (is_array($emailStructure->parts)) foreach ($emailStructure->parts as $part){
			switch ($part->type) {
				case 0:
					$text = wif_4forum_get_part($inbox, $mail, 1, $emailStructure->encoding);
					break;
				// Если картинка
				case 5:
					foreach ($part->dparameters as $img) {
						
						if (strpos($img->attribute,'filename')!==false) {
							$filename = $img->value;
							//Магия для обработки изображений
							$image = wif_4forum_get_part($inbox, $mail, $i, $emailStructure->encoding, true);
							$i += 1; //пермещаем указатель, я не знаю указатель ли это, но с этой штукой всё работает :)
							/////////////////////////////////
							$filetype = explode(utf8_encode('.'), utf8_encode($filename));
							$filetype = end($filetype);
							$data = base64_decode($image);
							
							$upload_dir = wp_upload_dir();
							$unique_filename = wp_unique_filename($upload_dir['path'], 'forum_pic.' . strtolower($filetype));
							$upload_file = wp_upload_bits($unique_filename, null, $data);
							$image_files[] = $upload_file;
						}
					}
					$img = null;
					break;
			}
		}
		if (empty($emailStructure->parts)){
			$text = wif_4forum_get_part($inbox, $mail, 1, $emailStructure->encoding);
		}
		//Формируем удобную структуру
		$message['text'] = $text;
		$message['files'] = $image_files;
		$message['from'] = $from;
		$massage['subject'] = $subject;
		//Расширяем массив
		$messages[] = $message;
		if (!imap_mail_move($inbox, "$mail:$mail", $to)) {
			wif_4forum_errs("Ошибка пермещения письма", "Проверть создана ли директория processed, а так же верно ли задан путь перемещения");
		}
		if ($last) {
			break;
		}
	}
	imap_expunge($inbox);
	imap_close($inbox);
	return $messages;
	
}

function wif_4forum_sync(){
	wif_4forum_errs('Начали синхронизацию вопросов...', ' ...');
	// Извлекаем настройки, которые нам нужны для работы с почтой...
	$settings_general = get_option( "wif_4forum_settings_general" );
	
	if (empty($settings_general)){
		return null;
	}
	//$settings_general['imap_path'] = '{imap.beget.ru:993/imap/ssl}INBOX';
	if ($settings_general['imap_path']=='' || $settings_general['email']=='' ||
	    $settings_general['pass']=='' || $settings_general['default-forum']==''){
		return null;
	}
	//Начинаем запрос новых
	$path = $settings_general['imap_path'];
	$username = $settings_general['email'];
	$pass = $settings_general['pass'];
	$last = $settings_general['last_one'];;
	$processed = $settings_general['imap_path_to'];
	
	$questions = wif_4forum_get_content_of_letter($username, $pass, $path, $processed, $last);
	//парсинг структуры
	
	$topics = array();
	
	foreach($questions as $question) {
		$from = $question['from'];
		//Если веньйо
		//echo $from['email'];
		//var_dump((mb_strpos($from['email'],'wordpress'));
		
		var_dump($from['name']);
		if (mb_strpos($from['name'], 'venyoo') || mb_strpos($from['email'], 'venyoo')) {
			$dict = wif_4forum_venyoo_parsing($question['text']);
		} elseif ($from['name'] == 'Заявка-на-ремонт.рф') {
			
			$dict = wif_4forum_zayavka_parcing($question['text']);
		} //Иначе если вопрос из формы
		else {
			$dict = wif_4forum_form_parsing($question['text']);
		}
		
		
		if ($dict !== null) {
			$title = $dict['subject:'];
			$message = $dict['message'];
			$user_email = $dict['email'];
		} else continue;
		
		
		//if (!$dict['name']) $user_name = $from['name'];
		//echo $dict['from'];
		if ($dict['from']) $user_name = $dict['from'];
		//elseif(mb_strpos($from['name'],'venyoo')||mb_strpos($from['email'],'venyoo')) $user_name = $dict['from'];
		else $user_name = $from['name'];
		
		$image_files = $question['files'];
		//Проверяем миниальный размер и если нет картинок. пропускаем сообщение мимо
		//if (mb_strlen($message)<$settings_general['min_length'] && !$image_files) continue;
		
		//Извлекаем данные автора
		$author = wif_4forum_get_author($user_email, $user_name);
		
		//Получаем раздел форума, надо будет сделать распределение по сайтам
		$forum = wif_4forum_get_forumsubject($message);
		
		if (empty($forum)) {
			$forum = $settings_general['default-forum'];
		}
		
		//Если всё-таки тема пустая
		if (mb_strlen($title) < 8 xor !$title) {
			$title = $message;
		}
		
		$title = wif_4forum_replace_greetings($title);
		//$message = wif_4forum_replace_greetings($message);
		
		if (mb_strlen($message) < $settings_general['min_length'] && !$image_files) continue;
		
		if (mb_strlen($title) > MAX_TITLE_SIZE) {
			
			$title = mb_substr($title, 0, MAX_TITLE_SIZE, 'UTF-8') . '...';
			
		}
		
		$title = mb_ucfirst($title, 'UTF-8');
		
		$topic_data = array(
			'post_parent' => $forum, // forum ID
			'post_status' => 'pending',
			'post_type' => bbp_get_topic_post_type(),
			'post_author' => $author->ID,
			'post_password' => '',
			'post_content' => $message,
			'post_title' => $title,
			'comment_status' => 'closed',
			'menu_order' => 0,
		);
		
		//Если атвор за данную сессию создавал уже вопрос объединяем их
		if ($topics[$author->ID] && $topics[$author->ID]['topic_data']['post_content'] !== $message) {
			$topics[$author->ID]['topic_data']['post_content'] .= (' ' . $message);
			$topics[$author->ID]['topic_data']['post_title'] .= (' ' . $title);
			
			if ($image_files){
				$topics[$author->ID]['files'] = $image_files;
			}
		}
		
		elseif (!$topics[$author->ID]) {
			$topics[$author->ID]['topic_data'] = $topic_data;
			
			if ($image_files){
				$topics[$author->ID]['files'] = $image_files;
			}
		}
		
	}
	
	//Два цикла были сделаны с целью фильтрации повторов
	
	foreach ($topics as $topic_i){
		
		$topic_data = $topic_i['topic_data'];
		$image_files = $topic_i['files'];
		
		$topic_id = bbp_insert_topic($topic_data);
		
		bbp_update_topic_forum_id( $topic_id, $forum );
		
		update_post_meta($topic_id, 'generated', date("Y-m-d H:i:s"));
		
		$topic = get_post($topic_id);
		
		//Подготовка заголовка письма
		$title = $topic->post_title;
		$subject = 'Ваш вопрос: "' . $title . '" - подтверждение размещения на форуме и ответ эксперта';
		
		//Если есть фотографии указываем в заголовке
		if ($image_files){
			$topic->post_title .= ' (фото)';
		}
		
		wp_update_post($topic);
		
		//Вставляем каждую фотку в сообщение и добавляем в БД WP
		foreach($image_files as $upload_file){
			if (!$upload_file['error']) {
				$filename = end(explode('/',$upload_file['file']));
				
				$wp_filetype = wp_check_filetype($filename, null );
				$attachment = array(
					'post_mime_type' => $wp_filetype['type'],
					'post_parent' => $topic_id,
					'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
					'post_content' => '',
					'post_status' => 'inherit',
					'post_author' => $topic_i->post_author,
				);
				$attachment_id = wp_insert_attachment( $attachment, $upload_file['file'], $topic_id );
				if (!is_wp_error($attachment_id)) {
					require_once(ABSPATH . "wp-admin" . '/includes/image.php');
					require_once(ABSPATH . 'wp-admin/includes/image.php');
					$attachment_data = wp_generate_attachment_metadata( $attachment_id, $upload_file['file'] );
					wp_update_attachment_metadata( $attachment_id,  $attachment_data );
					update_post_meta($attachment_id, '_bbp_attachment', '1');
				}
			}
		}
		
		$image_files = null;
		$topic_link = get_permalink($topic_id);
		
		//Обновление информации о последней активности
		bbp_update_forum_topic_count($forum);
		bbp_update_forum_last_topic_id($forum, $topic_id);
		bbp_update_forum_last_active_id($forum, $topic_id);
		bbp_update_forum_last_active_time($forum);
		
		//Письмо если новый человек с отправкой пароля
		
		
		if ($author->passwd_source){
			$letter = '<p>Здравствуйте, мы получили ваш вопрос:</p>
<p style="background: #f0d500; padding-left: 30px; padding-top: 10px; padding-bottom: 10px; font-weight:bold;">'.$message.'</p>
<p>Наши эксперты отвечают только на вопросы, опубликованные на нашем форуме.
 Для того, чтобы автоматически опубликовать вопрос на форуме, перейдите, пожалуйста, по ссылке
  <a href="'.$topic_link.'?wif_login='.$author->user_login.'&agree=yes&wif_pass='.$author->passwd_source.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=reg_letter&utm_content='.$author->user_email.'" style="color:#0171a1;">'.$topic_link .'?wif_login='.$author->user_login.'&agree=yes&wif_pass='.$author->passwd_source.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=reg_letter&utm_content='.$author->user_email.'
   </a>.
</p>
<p>Обычно время ответа экспертов от 5 минут до 2 суток (это полностью бесплатно и делается для развития сообщества и форума). Мы ничего не продаём и не оказываем никаких услуг.</p>
<!--Для удобства общения, мы создали Вам аккаунт:</p></div><footer style="background: #e6e6e6;height: 80px;margin-top: 20px; font-family: \'Helvetica Neue\', Arial, Helvetica, \'Nimbus Sans L\', sans-serif;font-size: 10pt; margin: 0 auto; text-align: center">
    <table cellpadding="0" cellspacing="0" style="border: solid 1px #e6e6e6;">
        <tr>
            <td style="border: solid 1px #e6e6e6; padding: 4px;">Логин:</td>
            <td style="border: solid 1px #e6e6e6; padding: 4px;">'.$author->user_login.'</td>
        </tr>
        <tr>
            <td style="border: solid 1px #e6e6e6; padding: 4px;">Пароль:</td>
            <td style="border: solid 1px #e6e6e6; padding: 4px;">'.$author->passwd_source.'</td>
        </tr>
    </table>-->';
			$footer = '<!--<p>Хотите получить ответ быстрее?</p>
<p style="font-weight: bold;">Отпишитесь о себе в теме для новичков форума: <a href="http://obustroeno.com/topic/49096?wif_login='.$author->user_login.'&agree=yes&wif_pass='.$author->passwd_source.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=reg_letter_znak&utm_content='.$author->user_email.'">http://obustroeno.com/topic/49096</a>. Так Вас быстрее заметят и Вы раньше получите ответ на вопрос.</p>
   -->
        <p>В случае проблем с регистрацией отпишитесь по адресу: support@obustroeno.com</p>
        <p>Если вы не хотите публиковать этот вопрос, просто проигнорируйте письмо, но тогда эксперты не смогут вам ответить.</p>
        <a href="'.$topic_link.'?wif_login='.$author->user_login.'&agree=yes&wif_pass='.$author->passwd_source.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=reg_letter&utm_content='.$author->user_email.'" style="position:relative; top: -23;font-weight: bold; width: 60%; background: #0171a1; display: inline-block; height: 40px; color: #fff; font-size: 14pt; position: relative; top: -10px; padding-top: 10px;padding-left: 10px;padding-right: 10px;">
            Подтвердить публикацию и  получить ответы экспертов
        </a><br>
        <span>С уважением, сообщество <a href="http://obustroeno.com?wif_login='.$author->user_login.'&agree=yes&wif_pass='.$author->passwd_source.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=reg_letter&utm_content='.$author->user_email.'" style="color:#0171a1;">Obustroeno.com</a></span>
    </footer>';
			
			
			
			wif_4forum_send_mail($user_email, $letter.$footer, $subject);
		}
		//Письмо если человек уже задавал вопрос
		
		else{
			$letter = '<p>Здравствуйте, мы получили ваш вопрос:</p>
<p style="background: #f0d500;  padding-left: 30px; padding-top: 10px; padding-bottom: 10px;">'.$message.'</p>
<p>Мы приняли решение опубликовать его на нашем профессиональном форуме <a href="'.$topic_link.'?utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=question_letter&utm_content='.$author->user_email.'" style="color:#0171a1;">форуме</a>.
Ответы специалистов и участников форума будут появляться по ссылке на <a href="'.$topic_link.'?utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=question_letter&utm_content='.$author->user_email.'" style="color:#0171a1;">'.$topic_link.'</a></p>
<p>Оповещения об ответах будут приходить дополнительно к Вам на почту. Обычно время ответа от 5 минут до 2 суток.</p>
                 <p  style="font-weight: bold;">Начните вести свой блог ремонта на проекте:<a href="http://obustroeno.com/new-record/?utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=question_letter_blog&utm_content='.$author->user_email.'" style="color:#0171a1;">http://obustroeno.com/new-record/</a></p>
                ';
			$footer = '</div><footer style="background: #e6e6e6;height: 80px;margin-top: 20px; font-family: \'Helvetica Neue\', Arial, Helvetica, \'Nimbus Sans L\', sans-serif;font-size: 10pt; margin: 0 auto; text-align: center">
        <a href="'.$topic_link.'?utm_source=email&utm_medium=email&utm_term=link3&utm_campaign=question_letter&utm_content='.$author->user_email.'" style="position:relative; top: -23;font-weight: bold; width: 60%; background: #0171a1; display: inline-block; height: 40px; color: #fff; font-size: 14pt; position: relative; top: -10px; padding-top: 10px;padding-left: 10px;padding-right: 10px;">
            Перейти в тему
        </a><br>
        <span>С уважением сообщество <a href="http://obustroeno.com?utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=question_letter&utm_content='.$author->user_email.'" style="color:#0171a1;">Obustroeno.com</a></span>
    </footer>';
			$stop_send_letters_flag = get_user_meta($author->ID, 'stop_send_letters', true);
			if (!$stop_send_letters_flag) {
				wif_4forum_send_mail($user_email, $letter . $footer, $subject);
			}
		}
	}
}



//Извлечение содрежимого из тела письма
function wif_4forum_get_part($connection, $messageNumber, $partNumber, $encoding, $file = false){
	$data = imap_fetchbody($connection, $messageNumber, $partNumber);
	//$data = imap_fetchbody($connection, $messageNumber, 1);
	
	//echo "<br>THE ENCODING IS -->$encoding<br>";
	//$data = imap_fetchbody($connection, $messageNumber, $partNumber, FT_UID);
	$message = $data;
	switch ($encoding) {
		case 0:
			//$data = mb_convert_encoding($data, "UTF-8", "auto");
			if (!$file) $data = decode7Bit($data);
			return $data; // 7BIT
			break;
		case 1:
			$data = quoted_printable_decode($data);
			return $data; // 8BIT
			break;
		case 2:
			return imap_binary($data); // BINARY
			break;
		case 3:
			return imap_base64($data); // BASE64
			break;
		case 4:
			return quoted_printable_decode($data); // QUOTED_PRINTABLE
		default:
			return $data; // OTHER
			break;
	}
}

// $part - часть письма, содержащий приложенный файл или само письмо
function wif_4forum_get_filename_from_part($part) {
	
	$filename = '';
	
	if($part->ifdparameters) {
		foreach($part->dparameters as $object) {
			if(strtolower($object->attribute) == 'filename') {
				$filename = $object->value;
			}
		}
	}
	
	if(!$filename && $part->ifparameters) {
		foreach($part->parameters as $object) {
			if(strtolower($object->attribute) == 'name') {
				$filename = $object->value;
			}
		}
	}
	return $filename;
}


function wif_4forum_get_forumsubject($message) {
	$last_id = get_option('wif_4forum_last_id');
	if (empty($last_id)) {
		return null;
	}
	for ($i=0; $i<$last_id; $i++){
		$option = get_option('wif_4forum_forum_' . $i);
		if ($option){
			$keys = explode(',',$option['keys']);
			if ($option['comparekey']=='1'){
				$all_keys = true;
				foreach ($keys as $key) {
					if (!strpos(utf8_encode($message), utf8_encode($key))){
						$all_keys = false;
						break;
					}
				}
				if ($all_keys) {
					return $option['forum_id'];
				}
			}
			else {
				foreach ($keys as $key) {
					if (strpos(utf8_encode($message), utf8_encode($key))){
						return $option['forum_id'];
					}
				}
			}
		}
	}
	return null;
}

function wif_4forum_get_author($email, $name){
	
	//Находим пользователя по его почте
	$user = get_user_by('email',$email);
	
	//Если нет такого пользователя регистрируем
	if (!$user) {
		$atts = explode(utf8_encode('@'), $email);
		$domains = explode(utf8_encode('.'), $atts[1]);
		
		if (get_user_by('user_login',$atts[0])){
			$login = $atts[0] . '_' .$domains[0].$domains[1];
		}
		else {
			$login = $atts[0];
		}
		$pass = wp_generate_password(12, false);
		$display_name = wif_4forum_get_display_name($login);
		
		$id = wp_create_user($login, $pass, $email);
		$user = get_userdata($id);
		$user->display_name = $display_name;
		update_user_meta($id, 'generated', date("Y-m-d H:i:s"));
		$user->add_role( 'subscriber' );
		$user->add_role( 'participant' );
		$user->passwd_source = $pass;
	}
	
	if ($user && $name && mb_strlen($name)>2 && (mb_strlen($user->first_name)<3|| !$user->first_name)) {
		$user->first_name = $name;
	}
	
	wp_update_user($user);
	return $user;
}

function wif_4forum_replace_ugly_names(){
	$users = get_users( [ 'role__in' => [ 'subscriber', 'participant' ] ] );
	foreach ($users as $user){
		$user->display_name = wif_4forum_get_display_name($user->user_login);
		wp_update_user($user);
		echo $user->display_name . '<br>';
	}
}

function wif_4forum_get_display_name($login){
	$symbols = array('.', ',', '_', '-', ':');
	
	$display_name = str_replace($symbols, ' ', $login);
	//var_dump($login);
	$display_name_arr = explode(" ", $display_name);
	//var_dump($display_name_arr);
	$display_name = '';
	
	foreach ($display_name_arr as $word){
		$display_name .= (mb_ucfirst($word) . ' ');
	}
	$display_name = substr($display_name, 0, -1);
	
	$number5 = substr($login, -5);
	$number4 = substr($login, -4);
	$number2 = substr($login, -2);
	$number1 = substr($login, -1);
	if ((int)$number5>0){
		return $display_name;
	}
	elseif ((int)$number4>0 && strlen(substr($display_name, 0, -4))>3){
		
		$display_name = substr($display_name, 0, -4);
	}
	elseif((int)$number2>0 && strlen(substr($display_name, 0, -2))>3){
		$display_name = substr($display_name, 0, -2);
	}
	elseif((int)$number1>0 && strlen(substr($display_name, 0, -1))>3){
		$display_name = substr($display_name, 0, -1);
	}
	
	return $display_name;
}


// =================================================
// Добавляем пункт меню на странице со списком плагинов

add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'add_action_links' );

function add_action_links ( $links ){
	$mylinks = array(
		'<a href="' . admin_url( 'options-general.php?page=wp_4forums%2Fincludes%2Fsettings.php' ) . '">Настройки</a>',
	);
	return array_merge( $links, $mylinks );
}


//if ( !function_exists('wp_new_user_notification') ) {
function wp_new_user_notification( $user_id, $plaintext_pass = '' ) {
	$user = new WP_User($user_id);
	
	$user_login = stripslashes($user->user_login);
	$user_email = stripslashes($user->user_email);
	
	$subject = 'Регистрация на сайте Обустроено';
	
	$letter = '<p>Здравствуйте, благодарим за регистрацию на сайте <a style="color:#0171a1;" href="http://obustroeno.com?utm_source=email&utm_medium=email&utm_term='.date('d/m/Y').'&utm_campaign=self_reg_letter&utm_content='.$user_email.'">Обустроено</a>!</p>

Вы создали аккаунт:</p></div><footer style="background: #e6e6e6;height: 80px;margin-top: 20px; font-family: \'Helvetica Neue\', Arial, Helvetica, \'Nimbus Sans L\', sans-serif;font-size: 10pt; margin: 0 auto; text-align: center">
    <table cellpadding="0" cellspacing="0" style="border: solid 1px #e6e6e6;">
        <tr>
            <td style="border: solid 1px #e6e6e6; padding: 4px;">Логин:</td>
            <td style="border: solid 1px #e6e6e6; padding: 4px;">'.$user_login.'</td>
        </tr>
        <tr>
            <td style="border: solid 1px #e6e6e6; padding: 4px;">Пароль:</td>
            <td style="border: solid 1px #e6e6e6; padding: 4px;">'.$plaintext_pass.'</td>
        </tr>
    </table>';
	$footer = '
         <p  style="font-weight: bold;">Начните вести свой блог ремонта на проекте: <a href="http://obustroeno.com/new-record/?wif_login='.$author->user_login.'&wif_pass='.$author->passwd_source.'&utm_source=email&utm_medium=email&utm_term='.get_the_date('d/m/Y', $topic_id).'&utm_campaign=reg_letter_topic_blog&utm_content='.$author->user_email.'" style="color:#0171a1;">http://obustroeno.com/new-record/</a>.
         </p>

        <p>Поменять пароль возможно в любое время, нажав на кнопку "вход" в верхнем правом углу сайта, затем нажав вкладку "Редактировать профиль"</p></div>
<footer style="background: #e6e6e6;height: 80px;margin-top: 20px; font-family: \'Helvetica Neue\', Arial, Helvetica, \'Nimbus Sans L\', sans-serif;font-size: 10pt; margin: 0 auto; text-align: center">
<span>С уважением, сообщество <a href="http://obustroeno.com?utm_source=email&utm_medium=email&utm_term='.date('d/m/Y').'&utm_campaign=self_reg_letter&utm_content='.$user_email.'" style="color:#0171a1;">Obustroeno.com</a></span>
    </footer>';
	
	
	
	wif_4forum_send_mail($user_email, $letter.$footer, $subject);
	
	// remove html content type
	//remove_filter ( 'wp_mail_content_type', 'wpmail_content_type' );
}
//}

function wif_4forum_psswd_title($title){
	return 'custom_title';
}


/*
apply_filters('retrieve_password_title', 'wif_4forum_psswd_title', 110, 1);
*/
add_action('init', 'wp_4forum_filters', 99);

function wp_4forum_filters(){
	add_filter("retrieve_password_message", "your_custom_message", 10, 4);
}

function your_custom_message($message, $key, $a, $b)    {
	$message = "your_custom_message";
	return $message;
}

/**
 * wpmail_content_type
 * allow html emails
 *
 * @author Joe Sexton <joe@webtipblog.com>
 * @return string
 */

function wpmail_content_type() {
	return 'text/html';
}